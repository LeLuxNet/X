package js

import (
	"runtime"
	"syscall/js"
)

var GOOS = runtime.GOOS

func init() {
	process := js.Global().Get("process")
	if process.IsUndefined() {
		return
	}

	platform := process.Get("platform")
	if platform.Type() != js.TypeString {
		return
	}

	GOOS = platform.String()
	switch GOOS {
	case "sunos":
		GOOS = "solaris"
	case "win32":
		GOOS = "windows"
	}
}
