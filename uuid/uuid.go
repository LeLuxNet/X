package uuid

import "github.com/google/uuid"

type UUID [16]byte

func (u UUID) String() string {
	return (uuid.UUID)(u).String()
}

func (u UUID) MarshalText() ([]byte, error) {
	return (uuid.UUID)(u).MarshalText()
}

func (u *UUID) UnmarshalText(b []byte) error {
	id, err := ParseBytes(b)
	if err != nil {
		return err
	}
	*u = id
	return nil
}

func (u UUID) MarshalBinary() ([]byte, error) {
	return u[:], nil
}

func (u *UUID) UnmarshalBinary(b []byte) error {
	return (*uuid.UUID)(u).UnmarshalBinary(b)
}

func Must(uuid UUID, err error) UUID {
	if err != nil {
		panic(err)
	}
	return uuid
}

func NewRandom() (UUID, error) {
	id, err := uuid.NewRandom()
	return UUID(id), err
}

func Parse(s string) (UUID, error) {
	id, err := uuid.Parse(s)
	return UUID(id), err
}

func ParseBytes(b []byte) (UUID, error) {
	id, err := uuid.ParseBytes(b)
	return UUID(id), err
}
