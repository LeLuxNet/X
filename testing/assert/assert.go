package assert

import (
	"bytes"
	"reflect"
	"testing"
	"time"
)

func Equal(t *testing.T, expected, got interface{}) {
	if !equal(expected, got) {
		t.Fatalf("\nexpected %#v,\n     got %#v", expected, got)
	}
}

func NotEqual(t *testing.T, expected, got interface{}) {
	if equal(expected, got) {
		t.Fatalf("did not expect %#v", expected)
	}
}

func equal(a, b interface{}) bool {
	if a == nil || b == nil {
		return a == b
	}

	return valueEqual(reflect.ValueOf(a), reflect.ValueOf(b))
}

var (
	timeType = reflect.TypeOf(time.Time{})
)

func valueEqual(a, b reflect.Value) bool {
	if !a.IsValid() || !b.IsValid() {
		return a.IsValid() == b.IsValid()
	}
	if a.Type() != b.Type() {
		return false
	}

	switch a.Kind() {
	case reflect.Array:
		for i := 0; i < a.Len(); i++ {
			if !valueEqual(a.Index(i), b.Index(i)) {
				return false
			}
		}
		return true
	case reflect.Slice:
		if a.IsNil() != b.IsNil() {
			return false
		}
		if a.Len() != b.Len() {
			return false
		}
		if a.UnsafePointer() == b.UnsafePointer() {
			return true
		}

		if a.Type().Elem().Kind() == reflect.Uint8 {
			return bytes.Equal(a.Bytes(), b.Bytes())
		}
		for i := 0; i < a.Len(); i++ {
			if !valueEqual(a.Index(i), b.Index(i)) {
				return false
			}
		}
		return true
	case reflect.Interface:
		if a.IsNil() || b.IsNil() {
			return a.IsNil() == b.IsNil()
		}
		return valueEqual(a.Elem(), b.Elem())
	case reflect.Pointer:
		if a.UnsafePointer() == b.UnsafePointer() {
			return true
		}
		return valueEqual(a.Elem(), b.Elem())
	case reflect.Struct:
		if a.Type() == timeType {
			return a.Interface().(time.Time).Equal(b.Interface().(time.Time))
		}
		for i, n := 0, a.NumField(); i < n; i++ {
			if !valueEqual(a.Field(i), b.Field(i)) {
				return false
			}
		}
		return true
	case reflect.Map:
		if a.IsNil() != b.IsNil() {
			return false
		}
		if a.Len() != b.Len() {
			return false
		}
		if a.UnsafePointer() == b.UnsafePointer() {
			return true
		}
		for _, k := range a.MapKeys() {
			av := a.MapIndex(k)
			bv := b.MapIndex(k)
			if !av.IsValid() || !bv.IsValid() || !valueEqual(av, bv) {
				return false
			}
		}
		return true
	case reflect.Func:
		return a.IsNil() && b.IsNil()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return a.Int() == b.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return a.Uint() == b.Uint()
	case reflect.String:
		return a.String() == b.String()
	case reflect.Bool:
		return a.Bool() == b.Bool()
	case reflect.Float32, reflect.Float64:
		return a.Float() == b.Float()
	case reflect.Complex64, reflect.Complex128:
		return a.Complex() == b.Complex()
	default:
		return a.Interface() == b.Interface()
	}
}
