package assert_test

import (
	"testing"
	"time"

	"lelux.net/x/testing/assert"
)

type st struct {
	a int
}

type st2 struct {
	b *int
}

var (
	time1 = time.Unix(10, 0)
	time2 = time.Unix(10, 0).In(time.FixedZone("", 5))
)

var n = 2

func TestEqual(t *testing.T) {
	assert.Equal(t, nil, nil)
	assert.Equal(t, 1, 1)
	assert.Equal(t, "abc", "abc")
	assert.Equal(t, []int{1, 2}, []int{1, 2})
	assert.Equal(t, []byte{5, 7}, []byte{5, 7})
	assert.Equal(t, time1, time1)
	assert.Equal(t, time1, time2)
	assert.Equal(t, &time1, &time2)
	assert.Equal(t, st{1}, st{1})
}

func TestNotEqual(t *testing.T) {
	assert.NotEqual(t, nil, 1)
	assert.NotEqual(t, 1, 2)
	assert.NotEqual(t, "abc", "aba")
	assert.NotEqual(t, []int{1, 2}, []int{1, 3})
	assert.NotEqual(t, []byte{5, 7}, []byte{5, 8})
	assert.NotEqual(t, time.Unix(0, 0), time.Unix(10, 0))
	assert.NotEqual(t, st{1}, st{2})
	assert.NotEqual(t, st2{nil}, st2{&n})
}
