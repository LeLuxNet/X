module lelux.net/x

go 1.16

require (
	github.com/andybalholm/brotli v1.0.4
	github.com/andybalholm/cascadia v1.3.1
	github.com/chai2010/webp v1.1.1
	github.com/google/uuid v1.3.0
	github.com/graphql-go/graphql v0.8.0
	github.com/klauspost/compress v1.15.15
	github.com/makiuchi-d/gozxing v0.1.1
	github.com/mjibson/go-dsp v0.0.0-20180508042940-11479a337f12
	github.com/nyaruka/phonenumbers v1.1.5
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
	github.com/vmihailenco/msgpack/v5 v5.3.5
	golang.org/x/image v0.3.0
	golang.org/x/net v0.5.0
	golang.org/x/xerrors v0.0.0-20240903120638-7835f813f4da // indirect
	nhooyr.io/websocket v1.8.7
)
