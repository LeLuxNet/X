package html_test

import (
	"testing"

	"lelux.net/x/html"
)

var unescaped = "& Δdelta <> lambdaλ some more text"
var escaped = `&amp; &#916;delta &lt;&gt; lambda&#x3bb; some more text`

func TestUnescape(t *testing.T) {
	s := html.UnescapeString(escaped)
	if s != unescaped {
		t.Fatalf(`expected to unescape to '%s', got '%s'`, s, unescaped)
	}
}

func BenchmarkEscape(b *testing.B) {
	html.EscapeString(unescaped)
	for i := 0; i < b.N; i++ {
		html.EscapeString(unescaped)
	}
}

func BenchmarkUnescape(b *testing.B) {
	html.EscapeString(unescaped)
	for i := 0; i < b.N; i++ {
		html.UnescapeString(escaped)
	}
}
