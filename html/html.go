package html

import (
	"io"

	"github.com/andybalholm/cascadia"
	"golang.org/x/net/html"
)

type Node html.Node

func Parse(r io.Reader) (*Node, error) {
	doc, err := html.Parse(r)
	return (*Node)(doc), err
}

func (n *Node) GetAttr(key string) string {
	for _, a := range n.Attr {
		if a.Key == key {
			return a.Val
		}
	}
	return ""
}

func (n *Node) Query(s Selector) *Node {
	return (*Node)(cascadia.Query((*html.Node)(n), s.s))
}

func (n *Node) QueryAll(s Selector) []*Node {
	cs := cascadia.QueryAll((*html.Node)(n), s.s)
	nodes := make([]*Node, len(cs))
	for i, c := range cs {
		nodes[i] = (*Node)(c)
	}
	return nodes
}
