package html

import (
	"strings"
	"unicode"
)

func TrimSpace(s string) string {
	space := false
	var b strings.Builder
	b.Grow(len(s))
	for _, r := range s {
		if unicode.IsSpace(r) {
			space = true
		} else {
			if space {
				b.WriteByte(' ')
			}
			b.WriteRune(r)
			space = false
		}
	}
	if space {
		b.WriteByte(' ')
	}
	return b.String()
}

var spaceEscaper = strings.NewReplacer(
	"\n", "<br>",
	"\t", "&nbsp;&nbsp;&nbsp;&nbsp;",
	"  ", "&nbsp; ",
)

func EscapeSpaceString(s string) string {
	return spaceEscaper.Replace(s)
}
