package html

import "github.com/andybalholm/cascadia"

type Selector struct {
	s cascadia.Sel
}

func (s Selector) String() string {
	return s.s.String()
}

func Compile(s string) (Selector, error) {
	sel, err := cascadia.Parse(s)
	return Selector{sel}, err
}

func MustCompile(s string) Selector {
	sel, err := Compile(s)
	if err != nil {
		panic(sel)
	}
	return sel
}
