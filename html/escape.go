package html

import (
	"html"
)

func EscapeString(s string) string {
	return html.EscapeString(s)
}

func UnescapeString(s string) string {
	return html.UnescapeString(s)
}
