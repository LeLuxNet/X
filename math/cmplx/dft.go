package cmplx

import "github.com/mjibson/go-dsp/fft"

func DFT(x []complex128) []complex128 {
	return fft.FFT(x)
}
