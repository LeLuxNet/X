// Package webp implements a WebP image decoder and encoder.
package webp

import (
	"image"
	"io"

	"golang.org/x/image/webp"
)

func DecodeConfig(r io.Reader) (image.Config, error) {
	return webp.DecodeConfig(r)
}

func Decode(r io.Reader) (image.Image, error) {
	return webp.Decode(r)
}

// No need to register the format as x/image/webp already does that by itsef
