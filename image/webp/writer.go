package webp

import (
	"image"
	"io"

	"github.com/chai2010/webp"
)

const DefaultQuality = 75

// Options are the encoding parameters.
// Quality ranges from 0 to 100 inclusive, higher is better.
// Exact preserves RGB values in transparent areas.
type Options struct {
	Lossless bool
	Quality  int
	Exact    bool
}

func Encode(w io.Writer, m image.Image, o *Options) error {
	quality := DefaultQuality
	if o != nil {
		quality = o.Quality
		if quality < 0 {
			quality = 0
		} else if quality > 100 {
			quality = 100
		}
	}

	return webp.Encode(w, m, &webp.Options{
		Lossless: o != nil && o.Lossless,
		Quality:  float32(quality),
		Exact:    o != nil && o.Exact,
	})
}
