package qoi_test

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"os"
	"testing"

	"lelux.net/x/image/qoi"
)

var filenames = []string{
	"dice",
	"kodim10",
	"kodim23",
	"qoi_logo",
	"testcard_rgba",
	"testcard",
	"wikipedia_008",
}

func diff(m0, m1 image.Image) error {
	b0, b1 := m0.Bounds(), m1.Bounds()
	if !b0.Size().Eq(b1.Size()) {
		return fmt.Errorf("dimensions differ: %v vs %v", b0, b1)
	}
	for y := b0.Min.Y; y < b0.Max.Y; y++ {
		for x := b0.Min.X; x < b0.Max.X; x++ {
			c0 := m0.At(x, y)
			c1 := m1.At(x, y)
			r0, g0, b0, a0 := c0.RGBA()
			r1, g1, b1, a1 := c1.RGBA()
			if r0 != r1 || g0 != g1 || b0 != b1 || a0 != a1 {
				return fmt.Errorf("colors differ at (%d, %d): %v vs %v", x, y, c0, c1)
			}
		}
	}
	return nil
}

func TestDecode(t *testing.T) {
	for _, fn := range filenames {
		qoiData, err := os.ReadFile("testdata/" + fn + ".qoi")
		if err != nil {
			t.Error(fn, err)
			continue
		}

		qoi, err := qoi.Decode(bytes.NewReader(qoiData))
		if err != nil {
			t.Error(fn, err)
			continue
		}

		pngData, err := os.ReadFile("testdata/" + fn + ".png")
		if err != nil {
			t.Error(fn, err)
			continue
		}

		png, err := png.Decode(bytes.NewReader(pngData))
		if err != nil {
			t.Error(fn, err)
			continue
		}

		err = diff(png, qoi)
		if err != nil {
			t.Error(fn, err)
			continue
		}
	}
}

func benchmarkDecode(b *testing.B, filename string) {
	data, err := os.ReadFile("testdata/" + filename + ".qoi")
	if err != nil {
		b.Fatal(err)
	}
	cfg, err := qoi.DecodeConfig(bytes.NewReader(data))
	if err != nil {
		b.Fatal(err)
	}
	b.SetBytes(int64(cfg.Width * cfg.Height * 4))
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		qoi.Decode(bytes.NewReader(data))
	}
}

func BenchmarkDecode(b *testing.B) {
	benchmarkDecode(b, "wikipedia_008")
}
