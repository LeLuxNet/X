// Package qoi implements a QOI image decoder and encoder.
//
// The QOI specification is at https://qoiformat.org/qoi-specification.pdf.
package qoi

import (
	"bufio"
	"encoding/binary"
	"errors"
	"image"
	"image/color"
	"io"
	"mime"
)

const magic = "qoif"

func decodeHeader(r io.Reader) (int, int, error) {
	header := make([]byte, 14)
	if _, err := io.ReadFull(r, header); err != nil {
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
		return 0, 0, err
	}

	if string(header[:4]) != magic {
		return 0, 0, errors.New("qoi: not a QOI file")
	}

	w := binary.BigEndian.Uint32(header[4:8])
	h := binary.BigEndian.Uint32(header[8:12])

	return int(w), int(h), nil
}

// DecodeConfig returns the color model and dimensions of a QOI image without decoding the entire image.
func DecodeConfig(r io.Reader) (image.Config, error) {
	w, h, err := decodeHeader(r)
	if err != nil {
		return image.Config{}, err
	}

	return image.Config{
		ColorModel: color.NRGBAModel,
		Width:      w,
		Height:     h,
	}, nil
}

// Decode reads a QOI image from r and returns it as an image.Image.
func Decode(r io.Reader) (image.Image, error) {
	w, h, err := decodeHeader(r)
	if err != nil {
		return nil, err
	}

	m := image.NewNRGBA(image.Rect(0, 0, w, h))

	d := decoder{
		r: bufio.NewReader(r),
		m: m,
	}
	if err = d.decode(); err != nil {
		return nil, err
	}
	return m, nil
}

const (
	opRGB  = 0xfe
	opRGBA = 0xff
)

const (
	opIndex = iota << 6
	opDiff
	opLuma
	opRun
)

var (
	firstCol = color.NRGBA{0, 0, 0, 0xff}
)

type decoder struct {
	r        io.ByteReader
	m        *image.NRGBA
	colCache colorCache
	lastCol  color.NRGBA
}

func (d *decoder) read() (byte, error) {
	b, err := d.r.ReadByte()
	if err == io.EOF {
		err = io.ErrUnexpectedEOF
	}
	return b, err
}

func (d *decoder) cacheLastCol() {
	d.colCache.add(d.lastCol)
}

func (d *decoder) rgb() error {
	var err error
	if d.lastCol.R, err = d.read(); err != nil {
		return err
	}
	if d.lastCol.G, err = d.read(); err != nil {
		return err
	}
	if d.lastCol.B, err = d.read(); err != nil {
		return err
	}
	d.cacheLastCol()
	return nil
}

func (d *decoder) rgba() error {
	var err error
	if d.lastCol.R, err = d.read(); err != nil {
		return err
	}
	if d.lastCol.G, err = d.read(); err != nil {
		return err
	}
	if d.lastCol.B, err = d.read(); err != nil {
		return err
	}
	if d.lastCol.A, err = d.read(); err != nil {
		return err
	}
	d.cacheLastCol()
	return nil
}

func (d *decoder) diff(b uint8) {
	d.lastCol.R += (b>>4)&3 - 2
	d.lastCol.G += (b>>2)&3 - 2
	d.lastCol.B += b&3 - 2
	d.cacheLastCol()
}

func (d *decoder) luma(b uint8) error {
	b2, err := d.read()
	if err != nil {
		return err
	}

	green := b&0x3f - 32
	d.lastCol.G += green

	green8 := green - 8
	d.lastCol.R += green8 + (b2>>4)&0x0f
	d.lastCol.B += green8 + b2&0x0f
	d.cacheLastCol()
	return nil
}

func (d *decoder) decode() error {
	d.lastCol = firstCol
	d.cacheLastCol()

	var b uint8
	var run uint8
	var err error

	for i := 0; i < len(d.m.Pix); i += 4 {
		if run > 0 {
			run--
		} else {
			if b, err = d.read(); err != nil {
				return err
			}

			switch b {
			case opRGB:
				err = d.rgb()
				if err != nil {
					return err
				}
			case opRGBA:
				err = d.rgba()
				if err != nil {
					return err
				}
			default:
				switch b & 0xc0 {
				case opIndex:
					d.lastCol = d.colCache[b]
				case opDiff:
					d.diff(b)
				case opLuma:
					err = d.luma(b)
					if err != nil {
						return err
					}
				case opRun:
					run = b & 0x3f
				}
			}
		}

		d.m.Pix[i] = d.lastCol.R
		d.m.Pix[i+1] = d.lastCol.G
		d.m.Pix[i+2] = d.lastCol.B
		d.m.Pix[i+3] = d.lastCol.A
	}

	return nil
}

func init() {
	image.RegisterFormat("qoi", magic, Decode, DecodeConfig)

	mime.AddExtensionType(".qoi", "image/qoi")
}
