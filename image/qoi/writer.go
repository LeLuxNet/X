package qoi

import (
	"encoding/binary"
	"fmt"
	"image"
	"image/color"
	"io"
)

func Encode(w io.Writer, m image.Image) error {
	e := encoder{
		w:   w,
		m:   m,
		buf: make([]byte, 10),
	}
	return e.encode()
}

type opaquer interface {
	Opaque() bool
}

// Returns whether or not the image is fully opaque.
func opaque(m image.Image) bool {
	if o, ok := m.(opaquer); ok {
		return o.Opaque()
	}
	b := m.Bounds()
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			_, _, _, a := m.At(x, y).RGBA()
			if a != 0xffff {
				return false
			}
		}
	}
	return true
}

type encoder struct {
	w        io.Writer
	m        image.Image
	colCache colorCache
	lastCol  color.NRGBA
	buf      []byte
	run      uint8
}

func (enc *encoder) setLastCol(c color.NRGBA) {
	enc.lastCol = c
	enc.colCache.add(enc.lastCol)
}

func (enc *encoder) write(n int) error {
	_, err := enc.w.Write(enc.buf[:n])
	return err
}

func (enc *encoder) wRun() error {
	enc.buf[0] = opRun | (enc.run - 1)
	enc.run = 0
	return enc.write(1)
}

func (enc *encoder) pix(c color.NRGBA) error {
	if c == enc.lastCol {
		enc.run++
		if enc.run == 62 {
			return enc.wRun()
		}
		return nil
	}

	if enc.run > 0 {
		if err := enc.wRun(); err != nil {
			return err
		}
	}

	if idx, ok := enc.colCache.get(c); ok {
		enc.buf[0] = opIndex | idx
		enc.lastCol = c
		return enc.write(1)
	}

	if c.A == enc.lastCol.A {
		vr := int8(c.R) - int8(enc.lastCol.R)
		vg := int8(c.G) - int8(enc.lastCol.G)
		vb := int8(c.B) - int8(enc.lastCol.B)

		if vr > -3 && vr < 2 &&
			vg > -3 && vg < 2 &&
			vb > -3 && vb < 2 {
			enc.buf[0] = opDiff | uint8(vr+2)<<4 | uint8(vg+2)<<2 | uint8(vb+2)
			enc.setLastCol(c)
			return enc.write(1)
		}

		vgr := vr - vg
		vgb := vb - vg

		if vgr > -9 && vgr < 8 &&
			vg > -33 && vg < 32 &&
			vgb > -9 && vgb < 8 {
			enc.buf[0] = opLuma | uint8(vg+32)
			enc.buf[1] = uint8(vgr+8)<<4 | uint8(vgb+8)
			enc.setLastCol(c)
			return enc.write(2)
		}

		enc.buf[0] = opRGB
		enc.buf[1] = c.R
		enc.buf[2] = c.G
		enc.buf[3] = c.B
		enc.setLastCol(c)
		return enc.write(4)
	}

	enc.buf[0] = opRGBA
	enc.buf[1] = c.R
	enc.buf[2] = c.G
	enc.buf[3] = c.B
	enc.buf[4] = c.A
	enc.setLastCol(c)
	return enc.write(5)
}

func (enc *encoder) encode() error {
	b := enc.m.Bounds()
	mw, mh := b.Dx(), b.Dy()
	if mw < 0 || mh < 0 {
		return fmt.Errorf("qoi: invalid image size: %dx%d", mw, mh)
	}

	_, err := io.WriteString(enc.w, magic)
	if err != nil {
		return err
	}

	binary.BigEndian.PutUint32(enc.buf, uint32(mw))
	binary.BigEndian.PutUint32(enc.buf[4:], uint32(mh))

	op := opaque(enc.m)
	if op {
		enc.buf[8] = 3
	} else {
		enc.buf[8] = 4
	}

	enc.buf[9] = 0

	if err := enc.write(10); err != nil {
		return err
	}

	enc.setLastCol(firstCol)

	if m, ok := enc.m.(*image.NRGBA); ok {
		for i := 0; i < len(m.Pix); i += 4 {
			c := color.NRGBA{m.Pix[i], m.Pix[i+1], m.Pix[i+2], m.Pix[i+3]}
			if err := enc.pix(c); err != nil {
				return err
			}
		}
	} else {
		for y := b.Min.Y; y < b.Max.Y; y++ {
			for x := b.Min.X; x < b.Max.X; x++ {
				c := color.NRGBAModel.Convert(enc.m.At(x, y)).(color.NRGBA)
				if err := enc.pix(c); err != nil {
					return err
				}
			}
		}
	}

	if enc.run > 0 {
		if err := enc.wRun(); err != nil {
			return err
		}
	}

	enc.buf[0] = 0
	enc.buf[1] = 0
	enc.buf[2] = 0
	enc.buf[3] = 0
	enc.buf[4] = 0
	enc.buf[5] = 0
	enc.buf[6] = 0
	enc.buf[7] = 1
	return enc.write(8)
}
