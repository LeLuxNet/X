package qoi

import "image/color"

type colorCache [64]color.NRGBA

func (cc *colorCache) add(c color.NRGBA) {
	cc[colorHash(c)] = c
}

func (cc *colorCache) get(c color.NRGBA) (uint8, bool) {
	i := colorHash(c)
	return i, cc[i] == c
}

func colorHash(c color.NRGBA) uint8 {
	return (c.R*3 + c.G*5 + c.B*7 + c.A*11) % 64
}
