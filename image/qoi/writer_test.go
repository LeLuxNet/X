package qoi_test

import (
	"bytes"
	"io"
	"os"
	"testing"

	"lelux.net/x/image/qoi"
)

func TestWriter(t *testing.T) {
	for _, fn := range filenames {
		data, err := os.ReadFile("testdata/" + fn + ".qoi")
		if err != nil {
			t.Error(fn, err)
			continue
		}

		m1, err := qoi.Decode(bytes.NewReader(data))
		if err != nil {
			t.Error(fn, err)
			continue
		}

		var m2buf bytes.Buffer
		err = qoi.Encode(&m2buf, m1)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		m2, err := qoi.Decode(bytes.NewReader(m2buf.Bytes()))
		if err != nil {
			t.Error(fn, err)
			continue
		}

		err = diff(m1, m2)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		// if !bytes.Equal(data, m2buf.Bytes()) {
		// 	t.Error(fn, "images are the same, bytes are different")
		// }
	}
}

func benchmarkEncode(b *testing.B, filename string) {
	data, err := os.ReadFile("testdata/" + filename + ".qoi")
	if err != nil {
		b.Fatal(err)
	}
	m, err := qoi.Decode(bytes.NewReader(data))
	if err != nil {
		b.Fatal(err)
	}
	b.SetBytes(int64(m.Bounds().Dx() * m.Bounds().Dy() * 4))
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		qoi.Encode(io.Discard, m)
	}
}

func BenchmarkEncode(b *testing.B) {
	benchmarkEncode(b, "wikipedia_008")
}
