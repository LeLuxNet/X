package color_test

import (
	"image/color"
	"testing"

	xcolor "lelux.net/x/image/color"
)

func TestHex(t *testing.T) {
	testCases := []struct {
		color color.Color
		hex   string
	}{
		{color.Black, "#000000"},
		{color.White, "#ffffff"},
		{color.RGBA{0x12, 0x34, 0x56, 0xff}, "#123456"},
	}

	for _, c := range testCases {
		if got, want := xcolor.Hex(c.color), c.hex; got != want {
			t.Fatalf("got %s, want %s", got, want)
		}
	}
}
