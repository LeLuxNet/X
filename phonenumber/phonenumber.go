package phonenumber

import (
	"github.com/nyaruka/phonenumbers"
)

type Type int

const (
	FixedLine Type = 1 << iota
	Mobile
	TollFree
	PremiumRate
	SharedCost
	VoIP
	PersonalNumber
	Pager
	UAN
	VoiceMail
)

func (t Type) convert() phonenumbers.PhoneNumberType {
	switch {
	case t&FixedLine&Mobile != 0:
		return phonenumbers.FIXED_LINE_OR_MOBILE
	case t&FixedLine != 0:
		return phonenumbers.FIXED_LINE
	case t&Mobile != 0:
		return phonenumbers.MOBILE
	case t&TollFree != 0:
		return phonenumbers.TOLL_FREE
	case t&PremiumRate != 0:
		return phonenumbers.PREMIUM_RATE
	case t&SharedCost != 0:
		return phonenumbers.SHARED_COST
	case t&VoIP != 0:
		return phonenumbers.VOIP
	case t&PersonalNumber != 0:
		return phonenumbers.PERSONAL_NUMBER
	case t&Pager != 0:
		return phonenumbers.PAGER
	case t&UAN != 0:
		return phonenumbers.UAN
	case t&VoiceMail != 0:
		return phonenumbers.VOICEMAIL
	default:
		return phonenumbers.UNKNOWN
	}
}

func mapToType(t phonenumbers.PhoneNumberType) Type {
	switch t {
	case phonenumbers.FIXED_LINE:
		return FixedLine
	case phonenumbers.MOBILE:
		return Mobile
	case phonenumbers.FIXED_LINE_OR_MOBILE:
		return FixedLine | Mobile
	case phonenumbers.UNKNOWN:
		return 0
	default:
		return 1 << (t - 1)
	}
}

type PhoneNumber struct {
	n *phonenumbers.PhoneNumber
}

func Parse(number, region string) (PhoneNumber, error) {
	var n PhoneNumber
	err := n.parse(number, region)
	return n, err
}

func Example(region string, typ Type) PhoneNumber {
	t := typ.convert()
	if t == phonenumbers.UNKNOWN {
		t = phonenumbers.FIXED_LINE
	}
	return PhoneNumber{phonenumbers.GetExampleNumberForType(region, t)}
}

func (n *PhoneNumber) parse(number, region string) error {
	if n.n == nil {
		n.n = &phonenumbers.PhoneNumber{}
	}
	return phonenumbers.ParseToNumber(number, region, n.n)
}

func (n PhoneNumber) Type() Type {
	return mapToType(phonenumbers.GetNumberType(n.n))
}

func (n PhoneNumber) IsPossible() bool {
	return phonenumbers.IsPossibleNumber(n.n)
}

func (n PhoneNumber) IsValid() bool {
	return phonenumbers.IsValidNumber(n.n)
}

func (n PhoneNumber) String() string {
	if n.n == nil {
		return ""
	}
	return phonenumbers.Format(n.n, phonenumbers.INTERNATIONAL)
}

func (n PhoneNumber) MarshalText() ([]byte, error) {
	if n.n == nil {
		return nil, nil
	}
	var b phonenumbers.Builder
	phonenumbers.FormatWithBuf(n.n, phonenumbers.E164, &b)
	return b.Bytes(), nil
}

func (n *PhoneNumber) UnmarshalText(text []byte) error {
	return n.parse(string(text), "")
}

// func (n PhoneNumber) MarshalBinary() ([]byte, error) {
// 	cc := *n.n.CountryCode
// 	nn := *n.n.NationalNumber

// 	return []byte{
// 		1,
// 		byte(cc >> 24),
// 		byte(cc >> 16),
// 		byte(cc >> 8),
// 		byte(cc),
// 		byte(nn >> 56),
// 		byte(nn >> 48),
// 		byte(nn >> 40),
// 		byte(nn >> 32),
// 		byte(nn >> 24),
// 		byte(nn >> 16),
// 		byte(nn >> 8),
// 		byte(nn),
// 	}, nil
// }

// func (n *PhoneNumber) UnmarshalBinary(data []byte) error {
// 	if len(data) == 0 {
// 		return errors.New("PhoneNumber.UnmarshalBinary: no data")
// 	}

// 	version := data[0]
// 	if version != 1 {
// 		return errors.New("PhoneNumber.UnmarshalBinary: unsupported version")
// 	}

// 	if len(data) != 1+4+8 {
// 		return errors.New("PhoneNumber.UnmarshalBinary: invalid length")
// 	}

// 	cc := int32(data[4]) | int32(data[3])<<8 | int32(data[2])<<16 | int32(data[1])<<24
// 	nn := uint64(data[12]) | uint64(data[11])<<8 | uint64(data[10])<<16 | uint64(data[9])<<24 |
// 		uint64(data[8])<<32 | uint64(data[7])<<40 | uint64(data[6])<<48 | uint64(data[5])<<56

// 	if n.n == nil {
// 		n.n = &phonenumbers.PhoneNumber{}
// 	}
// 	n.n.CountryCode = &cc
// 	n.n.NationalNumber = &nn

// 	return nil
// }
