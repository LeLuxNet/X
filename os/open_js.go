package os

import (
	"syscall/js"
)

func openApplication(url string) bool {
	o := js.Global().Get("open")
	if o.IsUndefined() {
		return false
	}

	return !o.Invoke(url, "_blank").IsNull()
}
