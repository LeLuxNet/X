package os

// Opens the url in the default application registered for that scheme.
func OpenApplication(url string) bool {
	return openApplication(url)
}
