// Package brotli implements the brotli compressed data format.
package brotli

import (
	"fmt"
	"io"

	"github.com/andybalholm/brotli"
)

const (
	BestSpeed          = 0
	BestCompression    = 11
	DefaultCompression = 6
)

type Reader struct {
	r *brotli.Reader
}

func NewReader(r io.Reader) Reader {
	return Reader{brotli.NewReader(r)}
}

// Read implements io.Reader, reading uncompressed bytes from its underlying Reader.
func (b Reader) Read(p []byte) (n int, err error) {
	return b.r.Read(p)
}

// Reset discards the Reader b's state and makes it equivalent to the
// result of its original state from NewReader, but reading from r instead.
// This permits reusing a Reader rather than allocating a new one.
func (b Reader) Reset(r io.Reader) {
	b.r.Reset(r)
}

type Writer struct {
	w *brotli.Writer
}

// NewWriter returns a new Writer. Writes to the returned writer are compressed and written to w.
//
// It is the caller's responsibility to call Close on the Writer when done. Writes may be buffered and not flushed until Close.
func NewWriter(w io.Writer) Writer {
	b, _ := NewWriterLevel(w, DefaultCompression)
	return b
}

// NewWriterLevel is like NewWriter but specifies the compression level instead
// of assuming DefaultCompression.
//
// The compression level can be DefaultCompression or any integer
// value between BestSpeed and BestCompression inclusive.
// The error returned will be nil if the level is valid.
func NewWriterLevel(w io.Writer, level int) (Writer, error) {
	if level < BestSpeed || level > BestCompression {
		return Writer{}, fmt.Errorf("brotli: invalid compression level: %d", level)
	}
	b := brotli.NewWriterLevel(w, level)
	return Writer{b}, nil
}

// Reset discards the Writer b's state and makes it equivalent to the
// result of its original state from NewWriter or NewWriterLevel, but
// writing to w instead. This permits reusing a Writer rather than
// allocating a new one.
func (b Writer) Reset(w io.Writer) {
	b.w.Reset(w)
}

// Write writes a compressed form of p to the underlying io.Writer. The
// compressed bytes are not necessarily flushed until the Writer is closed.
func (b Writer) Write(p []byte) (int, error) {
	return b.w.Write(p)
}

// Flush flushes any pending compressed data to the underlying writer.
//
// It is useful mainly in compressed network protocols, to ensure that
// a remote reader has enough data to reconstruct a packet. Flush does
// not return until the data has been written. If the underlying
// writer returns an error, Flush returns that error.
func (b Writer) Flush() error {
	return b.w.Flush()
}

// Close closes the Writer by flushing any unwritten data to the underlying io.Writer.
// It does not close the underlying io.Writer.
func (b Writer) Close() error {
	return b.w.Close()
}
