// Package snappy implements the snappy compressed data format.
package snappy

import (
	"io"

	"github.com/klauspost/compress/s2"
	"github.com/klauspost/compress/snappy"
)

type Reader struct {
	r *s2.Reader
}

func NewReader(r io.Reader) Reader {
	return Reader{snappy.NewReader(r)}
}

// Read implements io.Reader, reading uncompressed bytes from its underlying Reader.
func (s Reader) Read(p []byte) (n int, err error) {
	return s.r.Read(p)
}

func (s Reader) ReadByte() (byte, error) {
	return s.r.ReadByte()
}

// Reset discards the Reader s's state and makes it equivalent to the
// result of its original state from NewReader, but reading from r instead.
// This permits reusing a Reader rather than allocating a new one.
func (s Reader) Reset(r io.Reader) {
	s.r.Reset(r)
}

type Writer struct {
	w *s2.Writer
}

func NewWriter(w io.Writer) Writer {
	return Writer{snappy.NewBufferedWriter(w)}
}

// Reset discards the Writer s's state and makes it equivalent to the
// result of its original state from NewWriter, but writing to w instead.
// This permits reusing a Writer rather than allocating a new one.
func (s Writer) Reset(w io.Writer) {
	s.w.Reset(w)
}

// Write writes a compressed form of p to the underlying io.Writer. The
// compressed bytes are not necessarily flushed until the Writer is closed.
func (s Writer) Write(p []byte) (int, error) {
	return s.w.Write(p)
}

// Flush flushes any pending compressed data to the underlying writer.
//
// It is useful mainly in compressed network protocols, to ensure that
// a remote reader has enough data to reconstruct a packet. Flush does
// not return until the data has been written. If the underlying
// writer returns an error, Flush returns that error.
func (s Writer) Flush() error {
	return s.w.Flush()
}

// Close closes the Writer by flushing any unwritten data to the underlying io.Writer.
// It does not close the underlying io.Writer.
func (s Writer) Close() error {
	return s.w.Close()
}
