package textproto

import (
	"fmt"
	"io"
	"net/textproto"
	"sort"
)

type MIMEHeader textproto.MIMEHeader

func (h MIMEHeader) WriteTo(w io.Writer) (int64, error) {
	keys := make([]string, 0, len(h))
	for k := range h {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var n int64
	for _, k := range keys {
		for _, v := range h[k] {
			n2, err := fmt.Fprintf(w, "%s: %s\r\n", k, v)
			n += int64(n2)
			if err != nil {
				return n, err
			}
		}
	}
	n2, err := io.WriteString(w, "\r\n")
	n += int64(n2)
	return n, err
}
