package graphql

import (
	"encoding/json"
	"fmt"
	"io"
	"math/big"
	"net/url"
	"strconv"
	"strings"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

type ID string

type Number json.Number

var bigInt = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "BigInt",
	Description: "The `BigInt` scalar type represents non-fractional signed whole numeric values.",
	Serialize: func(v interface{}) interface{} {
		switch v := v.(type) {
		case big.Int:
			return v.String()
		case *big.Int:
			if v == nil {
				return nil
			}
			return v.String()
		case int64:
			return strconv.FormatInt(v, 10)
		case *int64:
			if v == nil {
				return nil
			}
			return strconv.FormatInt(*v, 10)
		case uint64:
			return strconv.FormatUint(v, 10)
		case *uint64:
			if v == nil {
				return nil
			}
			return strconv.FormatUint(*v, 10)
		default:
			return fmt.Sprint(v)
		}
	},
	ParseValue: func(v interface{}) interface{} {
		switch v := v.(type) {
		case string:
			return Number(v)
		default:
			return nil
		}
	},
	ParseLiteral: func(v ast.Value) interface{} {
		switch v := v.(type) {
		case *ast.StringValue:
			return Number(v.Value)
		case *ast.IntValue:
			return Number(v.Value)
		default:
			return nil
		}
	}})

var urlScalar = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "URL",
	Description: "", // TODO
	Serialize: func(v interface{}) interface{} {
		switch v := v.(type) {
		case url.URL:
			return v.String()
		case *url.URL:
			if v == nil {
				return nil
			}
			return v.String()
		default:
			return nil
		}
	},
	ParseValue: func(v interface{}) interface{} {
		switch v := v.(type) {
		case string:
			u, err := url.Parse(v)
			if err != nil {
				return nil
			}
			return u
		default:
			return nil
		}
	},
	ParseLiteral: func(v ast.Value) interface{} {
		switch v := v.(type) {
		case *ast.StringValue:
			u, err := url.Parse(v.Value)
			if err != nil {
				return nil
			}
			return u
		default:
			return nil
		}
	},
})

var upload = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "Upload",
	Description: "", // TODO
	Serialize: func(v interface{}) interface{} {
		return nil
	},
	ParseValue: func(v interface{}) interface{} {
		switch v := v.(type) {
		case string:
			return strings.NewReader(v)
		case io.Reader:
			return v
		default:
			return nil
		}
	},
	ParseLiteral: func(v ast.Value) interface{} {
		switch v := v.(type) {
		case *ast.StringValue:
			return strings.NewReader(v.Value)
		default:
			return nil
		}
	},
})
