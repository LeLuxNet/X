package graphql

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"sync"

	"github.com/graphql-go/graphql/gqlerrors"
)

// var AutomaticPersistedQuery = PersistedQuery{}
// var PersistedOperations = PersistedQuery{Store: operations, Required: true, Readonly: true}

type PersistedQuery struct {
	Store    sync.Map // map[hex encoded hash string]query string
	Required bool
	Readonly bool
}

func (e *PersistedQuery) PreParse(ctx context.Context, params *Params) error {
	pqVal, ok := params.Extensions["persistedQuery"]
	if !ok {
		return nil
	}

	pq, ok := pqVal.(map[string]interface{})
	if !ok || pq["version"] != 1. {
		return nil
	}

	hashVal, ok := pq["sha256Hash"]
	if !ok {
		return nil
	}
	hash, ok := hashVal.(string)
	if !ok {
		return nil
	}

	if params.Query != "" {
		sha := sha256.New()
		sha.Write([]byte(params.Query))
		hash2 := hex.EncodeToString(sha.Sum(nil))
		if hash != hash2 {
			return gqlerrors.NewFormattedError("Persisted Query Hash does not match")
		}
		if !e.Readonly {
			e.Store.Store(hash2, params.Query)
		}
		if !e.Required {
			return nil
		}
	}

	if e.Required && hash == "" {
		return gqlerrors.NewFormattedError("PersistedQueryOnly")
	}

	q, ok := e.Store.Load(hash)
	if ok {
		params.Query = q.(string)
		return nil
	}

	return gqlerrors.NewFormattedError("PersistedQueryNotFound")
}
