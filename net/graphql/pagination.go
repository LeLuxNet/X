package graphql

import (
	"encoding/base64"
	"encoding/binary"
	"errors"
	"reflect"

	"github.com/graphql-go/graphql"
)

type connection struct {
	Edges    []edge
	PageInfo PageInfo
}

type edge struct {
	Node   interface{}
	Cursor string
}

type PageInfo struct {
	HasPreviousPage bool   `graphql:"hasPreviousPage,nonnull"`
	HasNextPage     bool   `graphql:"hasNextPage,nonnull"`
	StartCursor     string `graphql:"startCursor"`
	EndCursor       string `graphql:"endCursor"`
}

var pageInfoType = reflect.TypeOf(PageInfo{})

var startCursor = marshalCursor(0)

func marshalCursor(i int) string {
	return base64.StdEncoding.EncodeToString(binary.AppendUvarint(nil, uint64(i)))
}

func unmarshalCursor(s string) (int, bool) {
	b, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return 0, false
	}
	i, n := binary.Uvarint(b)
	return int(i), n > 0
}

func paginationResolver(p graphql.ResolveParams, v interface{}) (interface{}, error) {
	list := reflect.ValueOf(v)
	l := list.Len()

	var after int
	if afterV, ok := p.Args["after"]; ok {
		after, ok = unmarshalCursor(afterV.(string))
		if !ok {
			return nil, errors.New("invalid cursor")
		}
		after++
		list = list.Slice(after, list.Len()-1)
	}

	if beforeV, ok := p.Args["before"]; ok {
		before, ok := unmarshalCursor(beforeV.(string))
		if !ok {
			return nil, errors.New("invalid cursor")
		}
		list = list.Slice(0, before)
	}

	if firstV, ok := p.Args["first"]; ok {
		first := firstV.(int)
		if first < 0 {
			return nil, errors.New("index out of range")
		}
		list = list.Slice(0, first)
	}

	if lastV, ok := p.Args["last"]; ok {
		last := lastV.(int)
		if last < 0 {
			return nil, errors.New("index out of range")
		}
		l := list.Len() - 1
		i := l - last
		list = list.Slice(i, l)
		after += i
	}

	edges := make([]edge, list.Len())
	for i := 0; i < list.Len(); i++ {
		edges[i] = edge{
			Node:   list.Index(i).Interface(),
			Cursor: marshalCursor(after + i)}
	}
	return connection{Edges: edges, PageInfo: PageInfo{
		StartCursor:     startCursor,
		EndCursor:       marshalCursor(l - 1),
		HasPreviousPage: after > 0,
		// HasNextPage:     // TODO
	}}, nil
}

func pagination(t graphql.Output, resolver func(p graphql.ResolveParams) (interface{}, error)) (*graphql.Field, error) {
	el := t.(*graphql.List).OfType
	name := el.(*graphql.NonNull).OfType.(*graphql.Object).Name()

	edges := graphql.NewList(graphql.NewObject(graphql.ObjectConfig{
		Name: name + "Edge",
		Fields: graphql.Fields{
			"node":   &graphql.Field{Type: el},
			"cursor": &graphql.Field{Type: graphql.NewNonNull(graphql.String)},
		}}))

	pageInfo, err := Type(pageInfoType)
	if err != nil {
		return nil, err
	}

	conn := graphql.NewObject(graphql.ObjectConfig{
		Name: name + "Connection",
		Fields: graphql.Fields{
			"edges":    &graphql.Field{Type: edges},
			"pageInfo": &graphql.Field{Type: pageInfo},
		}})

	return &graphql.Field{
		Type: conn,
		Args: graphql.FieldConfigArgument{
			"first":  &graphql.ArgumentConfig{Type: graphql.Int},
			"after":  &graphql.ArgumentConfig{Type: graphql.String},
			"last":   &graphql.ArgumentConfig{Type: graphql.Int},
			"before": &graphql.ArgumentConfig{Type: graphql.String},
		},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			v, err := resolver(p)
			if err != nil {
				return nil, err
			}
			return paginationResolver(p, v)
		},
	}, nil
}
