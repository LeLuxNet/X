package graphql

import (
	"github.com/graphql-go/graphql/gqlerrors"
)

type Error struct {
	Message    string                 `json:"message"`
	Locations  []ErrorLocation        `json:"locations,omitempty"`
	Path       []interface{}          `json:"path,omitempty"`
	Extensions map[string]interface{} `json:"extensions,omitempty"`
	Err        error                  `json:"-"`
}

func (e Error) Error() string {
	return e.Message
}

func (e Error) Unwrap() error {
	return e.Err
}

type ErrorLocation struct {
	Line   int `json:"line"`
	Column int `json:"column"`
}

func NewError(err error) Error {
	switch err := err.(type) {
	case Error:
		return err
	case gqlerrors.Error:
		err2 := Error{
			Message:   err.Message,
			Locations: make([]ErrorLocation, len(err.Locations)),
			Path:      err.Path,
			Err:       err.OriginalError}
		for i, l := range err.Locations {
			err2.Locations[i] = ErrorLocation(l)
		}
		if err := err.OriginalError; err != nil {
			if err, ok := err.(gqlerrors.ExtendedError); ok {
				err2.Extensions = err.Extensions()
			}
			// if err, ok := err.(stackTracer); ok {
			// 	if err2.Extensions == nil {
			// 		err2.Extensions = make(map[string]interface{})
			// 	}
			// 	err2.Extensions["stacktrace"] = err.StackTrace()
			// }
		}
		return err2
	case gqlerrors.FormattedError:
		err2 := Error{
			Message:    err.Message,
			Locations:  make([]ErrorLocation, len(err.Locations)),
			Path:       err.Path,
			Extensions: err.Extensions,
			Err:        err.OriginalError()}
		for i, l := range err.Locations {
			err2.Locations[i] = ErrorLocation(l)
		}
		// if err := err2.Err; err != nil {
		// 	if err, ok := err.(stackTracer); ok {
		// 		if err2.Extensions == nil {
		// 			err2.Extensions = make(map[string]interface{})
		// 		}
		// 		err2.Extensions["stacktrace"] = err.StackTrace()
		// 	}
		// }
		return err2
	case gqlerrors.ExtendedError:
		return Error{
			Message:    err.Error(),
			Extensions: err.Extensions(),
			Err:        err}
	default:
		return Error{
			Message: err.Error(),
			Err:     err}
	}
}
