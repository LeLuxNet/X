package graphql

import "github.com/graphql-go/graphql"

func mapResult(r *graphql.Result) Result {
	res := Result{
		Data:       r.Data,
		Errors:     make([]Error, len(r.Errors)),
		Extensions: r.Extensions}
	for i, err := range r.Errors {
		res.Errors[i] = NewError(err)
	}
	return res
}

type Result struct {
	Data       interface{}            `json:"data"`
	Errors     []Error                `json:"errors,omitempty"`
	Extensions map[string]interface{} `json:"extensions,omitempty"`
}
