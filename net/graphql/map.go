package graphql

import (
	"context"
	"encoding"
	"encoding/json"
	"fmt"
	"io"
	"math/big"
	"net/url"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

var typeMap sync.Map

func init() {
	RegisterType(reflect.TypeOf(ID("")), graphql.ID)
	RegisterType(reflect.TypeOf(time.Time{}), graphql.DateTime)
	RegisterType(reflect.TypeOf(big.Int{}), bigInt)
	RegisterType(reflect.TypeOf(url.URL{}), urlScalar)
	RegisterType(reflect.TypeOf((*io.Reader)(nil)).Elem(), upload)
}

type Output graphql.Output

func RegisterType(o reflect.Type, v Output) {
	typeMap.Store(o, v)
}

func RegisterEnum(o reflect.Type, name string, fields ...interface{}) {
	if len(fields)%2 != 0 {
		panic("graphql.RegisterEnum: odd fields count")
	}
	if name == "" {
		name = o.Name()
	}
	config := graphql.EnumConfig{
		Name:   name,
		Values: make(graphql.EnumValueConfigMap, len(fields)/2)}
	for i := 0; i < len(fields); i += 2 {
		name := fields[i+1].(string)
		val, err := reflectInterface(reflect.ValueOf(fields[i]), true)
		if err != nil {
			panic(fmt.Sprintf("graphql.RegisterEnum: marshal value: %v", err))
		}
		config.Values[name] = &graphql.EnumValueConfig{Value: val}
	}
	RegisterType(o, graphql.NewEnum(config))
}

func MustType(o reflect.Type) graphql.Output {
	res, err := Type(o)
	if err != nil {
		panic(err)
	}
	return res
}

type textMarshaler interface {
	encoding.TextMarshaler
	encoding.TextUnmarshaler
}

var textMarshalerType = reflect.TypeOf((*textMarshaler)(nil)).Elem()

type jsonMarshaler interface {
	json.Marshaler
	json.Unmarshaler
}

var jsonMarshalerType = reflect.TypeOf((*jsonMarshaler)(nil)).Elem()

func Type(o reflect.Type) (Output, error) {
	switch o.Kind() {
	case reflect.Pointer:
		o = o.Elem()
	}

	if res, ok := typeMap.Load(o); ok {
		return res.(graphql.Output), nil
	}

	var res graphql.Output
	op := reflect.PointerTo(o)
	if m, ok := op.MethodByName("MarshalGraphQL"); ok {
		if m.Type.NumOut() != 2 {
			return nil, fmt.Errorf("graphql: %v: MarshalGraphQL expects two return types", m.Type)
		}
		if !m.Type.Out(1).Implements(errorType) {
			return nil, fmt.Errorf("graphql: %v: MarshalGraphQL expects second return type to implement error", m.Type)
		}
		return Type(m.Type.Out(0))
	} else if op.Implements(textMarshalerType) {
		res = graphql.NewScalar(graphql.ScalarConfig{
			Name: o.Name(),
			Serialize: func(v interface{}) interface{} {
				m := v.(encoding.TextMarshaler)
				b, err := m.MarshalText()
				if err != nil {
					return nil
				}
				return string(b)
			},
			ParseValue: func(v interface{}) interface{} {
				switch value := v.(type) {
				case []byte:
					resV := reflect.New(o)
					res := resV.Interface().(textMarshaler)
					err := res.UnmarshalText(value)
					if err != nil {
						return nil
					}
					return resV.Elem().Interface()
				case string:
					resV := reflect.New(o)
					res := resV.Interface().(textMarshaler)
					err := res.UnmarshalText([]byte(value))
					if err != nil {
						return nil
					}
					return resV.Elem().Interface()
				default:
					return nil
				}
			},
			ParseLiteral: func(v ast.Value) interface{} {
				if v, ok := v.(*ast.StringValue); ok {
					resV := reflect.New(o)
					res := resV.Interface().(textMarshaler)
					err := res.UnmarshalText([]byte(v.Value))
					if err != nil {
						return nil
					}
					return resV.Elem().Interface()
				}
				return nil
			}})
	} else if op.Implements(jsonMarshalerType) {
		res = graphql.NewScalar(graphql.ScalarConfig{
			Name: o.Name(),
			Serialize: func(v interface{}) interface{} {
				m := v.(json.Marshaler)
				b, err := m.MarshalJSON()
				if err != nil {
					return nil
				}
				var v2 interface{}
				err = json.Unmarshal(b, &v2)
				if err != nil {
					return nil
				}
				return v2
			},
			ParseValue: func(v interface{}) interface{} {
				b, err := json.Marshal(v)
				if err != nil {
					return nil
				}
				resV := reflect.New(o)
				res := resV.Interface().(jsonMarshaler)
				err = res.UnmarshalJSON(b)
				if err != nil {
					return nil
				}
				return resV.Elem().Interface()
			},
			ParseLiteral: func(v ast.Value) interface{} {
				v2, err := mapLiteral(v)
				if err != nil {
					return nil
				}
				b, err := json.Marshal(v2)
				if err != nil {
					return nil
				}
				resV := reflect.New(o)
				res := resV.Interface().(jsonMarshaler)
				err = res.UnmarshalJSON(b)
				if err != nil {
					return nil
				}
				return resV.Elem().Interface()
			}})
	} else {
		switch o.Kind() {
		case reflect.String:
			res = graphql.String
		case reflect.Bool:
			res = graphql.Boolean
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32,
			reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32:
			res = graphql.Int
		case reflect.Int64, reflect.Uint64:
			res = bigInt
		case reflect.Float32, reflect.Float64:
			res = graphql.Float
		case reflect.Slice, reflect.Array:
			elem, err := Type(o.Elem())
			if err != nil {
				return nil, err
			}
			res = graphql.NewList(graphql.NewNonNull(elem))
		case reflect.Struct:
			var err error
			name := o.Name()
			for i := 0; i < o.NumField(); i++ {
				f := o.Field(i)
				if f.Type.AssignableTo(metaType) {
					name, _, _ = fieldName(f)
				}
			}
			resO := graphql.NewObject(graphql.ObjectConfig{
				Name: name,
				Fields: graphql.FieldsThunk(func() graphql.Fields {
					fields := make(graphql.Fields, o.NumField())
					err = structFields(o, fields)
					return fields
				})})
			RegisterType(o, resO)
			resO.Fields()
			if err != nil {
				typeMap.Delete(o)
				return nil, err
			}
			return resO, nil
		default:
			return nil, fmt.Errorf("graphql: %v: unsupported type: %s", o, o.Kind())
		}
	}

	RegisterType(o, res)
	return res, nil
}

var (
	errorType   = reflect.TypeOf((*error)(nil)).Elem()
	contextType = reflect.TypeOf((*context.Context)(nil)).Elem()
)

func inputStruct(t reflect.Type) (graphql.FieldConfigArgument, map[string]int, string, error) {
	args := make(graphql.FieldConfigArgument, t.NumField())
	argsMap := make(map[string]int, t.NumField())
	var structName string
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if f.Type.AssignableTo(metaType) {
			structName, _, _ = fieldName(f)
			continue
		}
		if !f.IsExported() {
			continue
		}
		t, err := Type(f.Type)
		if err != nil {
			return nil, nil, structName, err
		}
		name, nonnull, _ := fieldName(f)
		if nonnull {
			t = graphql.NewNonNull(t)
		}
		args[name] = &graphql.ArgumentConfig{Type: t}
		argsMap[name] = i
	}
	return args, argsMap, structName, nil
}

func callResolveFunc(m reflect.Method, context bool, argsT reflect.Type, argsMap map[string]int, p graphql.ResolveParams) (reflect.Value, error) {
	var self reflect.Value
	if _, ok := p.Source.(map[string]interface{}); ok || p.Source == nil {
		self = reflect.Zero(m.Type.In(0))
	} else {
		self = reflect.ValueOf(p.Source)
		if self.Kind() == reflect.Pointer {
			self = self.Elem()
		}
	}
	args := []reflect.Value{self}
	if context {
		args = append(args, reflect.ValueOf(p.Context))
	}
	if argsT != nil {
		v := reflect.New(argsT).Elem()
		for name, val := range p.Args {
			v.Field(argsMap[name]).Set(reflect.ValueOf(val))
		}
		args = append(args, v)
	}
	ret := m.Func.Call(args)
	if len(ret) > 1 && !ret[1].IsNil() {
		return ret[0], ret[1].Interface().(error)
	}
	return ret[0], nil
}

type Meta string

var metaType = reflect.TypeOf(Meta(""))

func structFields(o reflect.Type, fields graphql.Fields) error {
	for i := 0; i < o.NumMethod(); i++ {
		m := o.Method(i)

		if !m.IsExported() {
			continue
		}

		switch m.Type.NumOut() {
		case 1:
		case 2:
			if !m.Type.Out(1).Implements(errorType) {
				continue
			}
		default:
			continue
		}

		var args graphql.FieldConfigArgument
		var err error

		var context bool
		var argsT reflect.Type
		var argsMap map[string]int
		var name string
		switch m.Type.NumIn() {
		case 1:
		case 2:
			t := m.Type.In(1)
			if t.Implements(contextType) {
				context = true
			} else if t.Kind() == reflect.Struct {
				argsT = t
				args, argsMap, name, err = inputStruct(t)
			} else {
				continue
			}
		case 3:
			if m.Type.In(1).Implements(contextType) {
				context = true
			} else {
				continue
			}
			argsT = m.Type.In(2)
			if argsT.Kind() == reflect.Struct {
				args, argsMap, name, err = inputStruct(argsT)
			} else {
				continue
			}
		default:
			continue
		}
		if err != nil {
			return err
		}

		if name == "" {
			name = m.Name
		}

		ot := m.Type.Out(0)

		if ot.Kind() == reflect.Chan {
			rt, err := Type(ot.Elem())
			if err != nil {
				return err
			}

			fields[name] = &graphql.Field{
				Type: rt,
				Args: args,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return p.Source, nil
				},
				Subscribe: func(p graphql.ResolveParams) (interface{}, error) {
					ret, err := callResolveFunc(m, context, argsT, argsMap, p)
					if err != nil {
						return nil, err
					}
					if ret.IsNil() {
						return nil, nil
					}

					c := make(chan interface{})
					go func() {
						for {
							val, ok := ret.Recv()
							if !ok {
								close(c)
								return
							}
							c <- val.Interface()
						}
					}()
					return c, nil
				},
			}
		} else {
			rt, err := Type(ot)
			if err != nil {
				return err
			}

			_, enum := rt.(*graphql.Enum)

			fields[name] = &graphql.Field{
				Type: rt,
				Args: args,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					ret, err := callResolveFunc(m, context, argsT, argsMap, p)
					if err != nil {
						return nil, err
					}
					if !ret.CanInterface() {
						return nil, nil
					}
					return reflectInterface(ret, enum)
				},
			}
		}
	}
	for i := 0; i < o.NumField(); i++ {
		f := o.Field(i)

		t := f.Type
		switch t.Kind() {
		case reflect.Pointer:
			t = t.Elem()
		}

		if f.Anonymous && t.Kind() == reflect.Struct {
			err := structFields(t, fields)
			if err != nil {
				return err
			}
			continue
		}

		if !f.IsExported() || f.Type.AssignableTo(metaType) {
			continue
		}

		name, nonnull, pag := fieldName(f)
		if name == "" {
			continue
		}
		fieldO, err := Type(t)
		if err != nil {
			return err
		}
		if nonnull {
			fieldO = graphql.NewNonNull(fieldO)
		}
		if pag {
			fields[name], err = pagination(fieldO, defaultResolver)
			if err != nil {
				return err
			}
		} else {
			fields[name] = &graphql.Field{Type: fieldO, Resolve: defaultResolver}
		}
	}
	return nil
}

func reflectInterface(v reflect.Value, enum bool) (interface{}, error) {
	for {
		m, ok := v.Type().MethodByName("MarshalGraphQL")
		if !ok {
			break
		}
		out := m.Func.Call([]reflect.Value{v})
		v = out[0]
	}

	if v.Kind() == reflect.Pointer {
		if v.IsNil() {
			return nil, nil
		}
		v = v.Elem()
	}

	if enum {
		switch v.Kind() {
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			return int(v.Uint()), nil
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			return int(v.Int()), nil
		default:
			panic("expected enum compatible type")
		}
	}

	switch v.Kind() {
	case reflect.Bool:
		return v.Bool(), nil

	case reflect.Uint:
		return uint(v.Uint()), nil
	case reflect.Uint8:
		return uint8(v.Uint()), nil
	case reflect.Uint16:
		return uint16(v.Uint()), nil
	case reflect.Uint32:
		return uint32(v.Uint()), nil
	case reflect.Uint64:
		return v.Uint(), nil

	case reflect.Int:
		return int(v.Int()), nil
	case reflect.Int8:
		return int8(v.Int()), nil
	case reflect.Int16:
		return int16(v.Int()), nil
	case reflect.Int32:
		return int32(v.Int()), nil
	case reflect.Int64:
		return v.Int(), nil

	case reflect.Float32:
		return float32(v.Float()), nil
	case reflect.Float64:
		return v.Float(), nil

	case reflect.Complex64:
		return complex64(v.Complex()), nil
	case reflect.Complex128:
		return v.Complex(), nil

	case reflect.String:
		return v.String(), nil

	default:
		return v.Interface(), nil
	}
}

func defaultResolver(p graphql.ResolveParams) (interface{}, error) {
	v := reflect.ValueOf(p.Source)
	if v.Type().Kind() == reflect.Pointer {
		v = v.Elem()
	}

	switch v.Kind() {
	case reflect.Struct:
		return resolveStruct(p, v)
	}

	return graphql.DefaultResolveFn(p)
}

func fieldName(t reflect.StructField) (string, bool, bool) {
	tag := t.Tag.Get("graphql")
	if tag != "" {
		var end string
		tag, end, _ = strings.Cut(tag, ",")
		if tag == "-" {
			tag = ""
		}
		return tag, end == "nonnull", end == "pagination"
	}
	tag = t.Tag.Get("json")
	if tag != "" {
		tag, _, _ = strings.Cut(tag, ",")
		if tag == "-" {
			tag = ""
		}
		return tag, false, false
	}
	return t.Name, false, false
}

func resolveStruct(p graphql.ResolveParams, v reflect.Value) (interface{}, error) {
	t := v.Type()
	for i := 0; i < v.NumField(); i++ {
		f := t.Field(i)

		vf := v.Field(i)
		if vf.Type().Kind() == reflect.Pointer {
			vf = vf.Elem()
		}

		if f.Anonymous && vf.Type().Kind() == reflect.Struct {
			res, err := resolveStruct(p, vf)
			if res != nil || err != nil {
				return res, err
			}
		} else {
			name, _, _ := fieldName(f)
			if name != "" && name == p.Info.FieldName {
				vf := v.Field(i)
				_, enum := MustType(vf.Type()).(*graphql.Enum)
				return reflectInterface(vf, enum)
			}
		}
	}
	return nil, nil
}
