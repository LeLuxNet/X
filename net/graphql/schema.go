package graphql

import (
	"context"
	"reflect"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
	"github.com/graphql-go/graphql/language/parser"
)

type Schema struct {
	Schema     graphql.Schema
	Extensions []Extension
}

func NewSchema(query, mutation, subscription interface{}) (Schema, error) {
	conf := graphql.SchemaConfig{
		Extensions: []graphql.Extension{schemaExtInstance}}

	if query != nil {
		t, err := Type(reflect.TypeOf(query))
		if err != nil {
			return Schema{}, err
		}
		conf.Query = t.(*graphql.Object)
	} else {
		conf.Query = graphql.NewObject(graphql.ObjectConfig{
			Name: "Query",
			Fields: graphql.Fields{
				"_placeholder": &graphql.Field{
					Type: graphql.String,
				}}})
	}

	if mutation != nil {
		t, err := Type(reflect.TypeOf(mutation))
		if err != nil {
			return Schema{}, err
		}
		conf.Mutation = t.(*graphql.Object)
	}

	if subscription != nil {
		t, err := Type(reflect.TypeOf(subscription))
		if err != nil {
			return Schema{}, err
		}
		conf.Subscription = t.(*graphql.Object)
	}

	s, err := graphql.NewSchema(conf)
	return Schema{Schema: s}, err
}

func (s Schema) extsInit(ctx context.Context, params *Params) (context.Context, []Extension, *ast.Document, []Error) {
	exts := make([]Extension, len(s.Extensions))
	for i, ext := range s.Extensions {
		if extCon, ok := ext.(func() Extension); ok {
			ext = extCon()
		}
		exts[i] = ext

		if in, ok := ext.(preParseExt); ok {
			err := in.PreParse(ctx, params)
			if err != nil {
				return ctx, exts, nil, []Error{NewError(err)}
			}
		}
	}

	ctx = context.WithValue(ctx, schemaExtInstance, exts)

	a, err := parser.Parse(parser.ParseParams{Source: params.Query})
	if err != nil {
		return ctx, exts, a, []Error{NewError(err)}
	}

	for i := len(exts) - 1; i >= 0; i-- {
		ext := exts[i]
		if ext, ok := ext.(postParseExt); ok {
			err := ext.PostParse(ctx, a)
			if err != nil {
				return ctx, exts, a, []Error{NewError(err)}
			}
		}
	}

	for _, ext := range exts {
		if ext, ok := ext.(preValidationExt); ok {
			err := ext.PreValidation(ctx)
			if err != nil {
				return ctx, exts, a, []Error{NewError(err)}
			}
		}
	}

	valid := graphql.ValidateDocument(&s.Schema, a, nil)
	if !valid.IsValid {
		errs := make([]Error, len(valid.Errors))
		for i, err := range valid.Errors {
			errs[i] = NewError(err)
		}
		return ctx, exts, a, errs
	}

	for i := len(exts) - 1; i >= 0; i-- {
		ext := exts[i]
		if ext, ok := ext.(postValidationExt); ok {
			err := ext.PostValidation(ctx)
			if err != nil {
				return ctx, exts, a, []Error{NewError(err)}
			}
		}
	}

	for _, ext := range exts {
		if ext, ok := ext.(preExecutionExt); ok {
			err := ext.PreExecution(ctx)
			if err != nil {
				return ctx, exts, a, []Error{NewError(err)}
			}
		}
	}

	return ctx, exts, a, nil
}

func (s Schema) Do(ctx context.Context, params Params) Result {
	ctx, exts, ast, errs := s.extsInit(ctx, &params)
	if len(errs) > 0 {
		return Result{Errors: errs}
	}

	res := mapResult(graphql.Execute(graphql.ExecuteParams{
		Schema:        s.Schema,
		AST:           ast,
		OperationName: params.OperationName,
		Args:          params.Variables,
		Context:       ctx,
	}))

	for i := len(exts) - 1; i >= 0; i-- {
		ext := exts[i]
		if ext, ok := ext.(postExecutionExt); ok {
			err := ext.PostExecution(ctx, &res)
			if err != nil {
				return Result{Errors: []Error{NewError(err)}}
			}
		}
	}

	return res
}

func (s Schema) Subscribe(ctx context.Context, params Params) <-chan Result {
	ctx, exts, ast, errs := s.extsInit(ctx, &params)
	if len(errs) > 0 {
		c := make(chan Result, 1)
		c <- Result{Errors: errs}
		return c
	}

	c := graphql.ExecuteSubscription(graphql.ExecuteParams{
		Schema:        s.Schema,
		AST:           ast,
		OperationName: params.OperationName,
		Args:          params.Variables,
		Context:       ctx,
	})

	for i := len(exts) - 1; i >= 0; i-- {
		ext := exts[i]
		if ext, ok := ext.(postExecutionExt); ok {
			err := ext.PostExecution(ctx, nil)
			if err != nil {
				c := make(chan Result, 1)
				c <- Result{Errors: []Error{NewError(err)}}
				return c
			}
		}
	}

	c2 := make(chan Result)
	go func() {
		for res := range c {
			c2 <- mapResult(res)
		}
		close(c2)
	}()
	return c2
}

type schemaExt struct{}

var schemaExtInstance = schemaExt{}

func (schemaExt) Init(ctx context.Context, _ *graphql.Params) context.Context {
	return ctx
}

func (schemaExt) Name() string {
	return ""
}

func (schemaExt) ParseDidStart(ctx context.Context) (context.Context, graphql.ParseFinishFunc) {
	return ctx, nil
}

func (schemaExt) ValidationDidStart(ctx context.Context) (context.Context, graphql.ValidationFinishFunc) {
	return ctx, nil
}

func (schemaExt) ExecutionDidStart(ctx context.Context) (context.Context, graphql.ExecutionFinishFunc) {
	return ctx, func(r *graphql.Result) {}
}

func (schemaExt) ResolveFieldDidStart(ctx context.Context, info *graphql.ResolveInfo) (context.Context, graphql.ResolveFieldFinishFunc) {
	exts := ctx.Value(schemaExtInstance).([]Extension)

	for _, ext := range exts {
		if ext, ok := ext.(preFieldResolverExt); ok {
			err := ext.PreFieldResolver(ctx, info)
			if err != nil {
				panic(err)
			}
		}
	}

	return ctx, func(i interface{}, err error) {
		for i := len(exts) - 1; i >= 0; i-- {
			ext := exts[i]
			if ext, ok := ext.(postFieldResolverExt); ok {
				err := ext.PostFieldResolver(ctx, info, &i)
				if err != nil {
					panic(err)
				}
			}
		}
	}
}

func (schemaExt) HasResult() bool {
	return false
}

func (schemaExt) GetResult(ctx context.Context) interface{} {
	return nil
}
