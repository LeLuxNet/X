package graphql

import (
	"context"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

type Extension interface{}

type preParseExt interface {
	PreParse(ctx context.Context, params *Params) error
}

type postParseExt interface {
	PostParse(ctx context.Context, ast *ast.Document) error
}

type preValidationExt interface {
	PreValidation(ctx context.Context) error
}

type postValidationExt interface {
	PostValidation(ctx context.Context) error
}

type preFieldResolverExt interface {
	PreFieldResolver(ctx context.Context, info *graphql.ResolveInfo) error
}

type postFieldResolverExt interface {
	PostFieldResolver(ctx context.Context, info *graphql.ResolveInfo, val interface{}) error
}

type preExecutionExt interface {
	PreExecution(ctx context.Context) error
}

type postExecutionExt interface {
	PostExecution(ctx context.Context, res *Result) error
}
