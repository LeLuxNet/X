package graphql

import (
	"encoding/json"
	"net/url"
)

type Params struct {
	Query         string                 `json:"query"`
	OperationName string                 `json:"operationName"`
	Variables     map[string]interface{} `json:"variables"`
	Extensions    map[string]interface{} `json:"extensions"`
}

func formParams(v url.Values) (Params, error) {
	params := Params{
		Query:         v.Get("query"),
		OperationName: v.Get("operationName")}

	s := v.Get("variables")
	if s != "" {
		err := json.Unmarshal([]byte(s), &params.Variables)
		if err != nil {
			return params, err
		}
	}
	s = v.Get("extensions")
	if s != "" {
		err := json.Unmarshal([]byte(s), &params.Extensions)
		if err != nil {
			return params, err
		}
	}

	return params, nil
}
