package graphql

import (
	"context"
	"sync"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

func Tracing() Extension {
	return &tracing{}
}

type tracing struct {
	Version    int              `json:"version"`
	StartTime  time.Time        `json:"startTime"`
	EndTime    time.Time        `json:"endTime"`
	Duration   time.Duration    `json:"duration"`
	Parsing    tracingSpan      `json:"parsing"`
	Validation tracingSpan      `json:"validation"`
	Execution  tracingExecution `json:"execution"`
}

type tracingExecution struct {
	Resolvers []tracingResolver `json:"resolvers"`
	resolvers sync.Map
}

type tracingSpan struct {
	startTime   time.Time
	StartOffset time.Duration `json:"startOffset"`
	Duration    time.Duration `json:"duration"`
}

type tracingResolver struct {
	Path       []interface{} `json:"path"`
	ParentType string        `json:"parentType"`
	FieldName  string        `json:"fieldName"`
	ReturnType string        `json:"returnType"`
	tracingSpan
}

func (e *tracing) PreParse(ctx context.Context, params *Params) error {
	e.Version = 1
	e.StartTime = time.Now()
	e.Parsing = tracingSpan{startTime: e.StartTime}
	return nil
}

func (e *tracing) PostParse(ctx context.Context, ast *ast.Document) error {
	e.Parsing.Duration = time.Since(e.Parsing.startTime)
	return nil
}

func (e *tracing) PreValidation(ctx context.Context) error {
	now := time.Now()
	e.Validation = tracingSpan{startTime: now, StartOffset: now.Sub(e.StartTime)}
	return nil
}

func (e *tracing) PostValidation(ctx context.Context) error {
	e.Validation.Duration = time.Since(e.Validation.startTime)
	return nil
}

func (e *tracing) PreFieldResolver(ctx context.Context, info *graphql.ResolveInfo) error {
	now := time.Now()
	res := tracingResolver{
		Path:        info.Path.AsArray(),
		ParentType:  info.ParentType.String(),
		ReturnType:  info.ReturnType.String(),
		FieldName:   info.FieldName,
		tracingSpan: tracingSpan{startTime: now, StartOffset: now.Sub(e.StartTime)}}
	e.Execution.resolvers.Store(info, res)
	return nil
}

func (e *tracing) PostFieldResolver(ctx context.Context, info *graphql.ResolveInfo, val interface{}) error {
	resV, _ := e.Execution.resolvers.LoadAndDelete(info)
	res := resV.(tracingResolver)
	res.Duration = time.Since(res.startTime)
	e.Execution.Resolvers = append(e.Execution.Resolvers, res)
	return nil
}

func (e *tracing) PostExecution(ctx context.Context, res *Result) error {
	e.EndTime = time.Now()
	e.Duration = e.EndTime.Sub(e.StartTime)

	if res.Extensions == nil {
		res.Extensions = make(map[string]interface{})
	}
	res.Extensions["tracing"] = e

	return nil
}
