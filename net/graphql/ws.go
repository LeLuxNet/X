package graphql

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

// https://github.com/enisdenjo/graphql-ws/blob/master/PROTOCOL.md

var wsOpts = &websocket.AcceptOptions{Subprotocols: []string{"graphql-transport-ws"}}

type wsMessage struct {
	Type    string          `json:"type"`
	Id      string          `json:"id"`
	Payload json.RawMessage `json:"payload"`
}

type wsMessageSend struct {
	Type    string      `json:"type"`
	Id      string      `json:"id,omitempty"`
	Payload interface{} `json:"payload,omitempty"`
}

type ws struct {
	ctx    context.Context
	conn   *websocket.Conn
	schema Schema
	ack    bool
	subs   sync.Map
}

func (w *ws) run() {
	defer func() {
		w.subs.Range(func(key, val interface{}) bool {
			val.(context.CancelFunc)()
			return true
		})
	}()

	go func() {
		time.Sleep(10 * time.Second)
		if !w.ack {
			w.conn.Close(4408, "Connection initialisation timeout")
		}
	}()

	for {
		var msg wsMessage
		err := wsjson.Read(w.ctx, w.conn, &msg)
		if err != nil {
			w.conn.Close(4400, "Reading JSON message failed")
			return
		}

		if w.handleMsg(msg) {
			return
		}
	}
}

func (w *ws) handleMsg(msg wsMessage) bool {
	switch msg.Type {
	case "connection_init":
		w.ack = true
		wsjson.Write(w.ctx, w.conn, wsMessageSend{Type: "connection_ack"})
	case "ping":
		wsjson.Write(w.ctx, w.conn, wsMessageSend{Type: "pong"})
	case "pong":
	case "subscribe":
		if !w.ack {
			w.conn.Close(4401, "Unauthorized")
			return true
		}

		var params Params
		err := json.Unmarshal(msg.Payload, &params)
		if err != nil {
			w.conn.Close(4400, "Parsing JSON payload failed")
			return true
		}

		ctx, cancel := context.WithCancel(context.Background())

		if _, ok := w.subs.LoadOrStore(msg.Id, cancel); ok {
			w.conn.Close(4409, fmt.Sprintf("Subscriber for %s already exists", msg.Id))
			return true
		}

		go w.subscribe(ctx, cancel, msg.Id, params)
	case "complete":
		if cancel, ok := w.subs.Load(msg.Id); ok {
			cancel.(context.CancelFunc)()
		}
	default:
		w.conn.Close(4400, "Unknown type "+msg.Type)
		return true
	}
	return false
}

func (w *ws) subscribe(ctx context.Context, cancel context.CancelFunc, id string, params Params) {
	c := w.schema.Subscribe(ctx, params)
	done := ctx.Done()

loop:
	for {
		select {
		case <-done:
			break loop
		case res, ok := <-c:
			if !ok {
				break loop
			}
			wsjson.Write(ctx, w.conn, wsMessageSend{
				Type:    "next",
				Id:      id,
				Payload: res})
		}
	}

	w.subs.Delete(id)
	cancel()
	wsjson.Write(w.ctx, w.conn, wsMessageSend{Type: "complete", Id: id})
}
