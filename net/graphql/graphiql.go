package graphql

import (
	"embed"
	"html/template"
	"net/http"
)

//go:embed graphiql.html
var html embed.FS

var t = template.Must(template.ParseFS(html, "graphiql.html"))

type GraphiQL struct {
	URL             string
	SubscriptionURL string
}

func (h GraphiQL) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.Execute(w, h)
}
