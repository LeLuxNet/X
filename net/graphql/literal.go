package graphql

import (
	"encoding/json"
	"errors"

	"github.com/graphql-go/graphql/language/ast"
)

func mapLiteral(v ast.Value) (interface{}, error) {
	switch v := v.(type) {
	case *ast.Variable:
		return nil, errors.New("unexpected variable")
	case *ast.IntValue:
		return json.Number(v.Value), nil
	case *ast.FloatValue:
		return json.Number(v.Value), nil
	case *ast.StringValue:
		return v.Value, nil
	case *ast.BooleanValue:
		return v.Value, nil
	case *ast.ListValue:
		l := make([]interface{}, len(v.Values))
		var err error
		for i, v := range v.Values {
			l[i], err = mapLiteral(v)
			if err != nil {
				return nil, err
			}
		}
		return l, nil
	case *ast.ObjectValue:
		o := make(map[string]interface{}, len(v.Fields))
		var err error
		for _, f := range v.Fields {
			o[f.Name.Value], err = mapLiteral(f.Value)
			if err != nil {
				return nil, err
			}
		}
		return o, nil
	default:
		return nil, errors.New("unsupported ast type")
	}
}
