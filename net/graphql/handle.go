package graphql

import (
	"context"
	"encoding/json"
	"io"
	"mime"
	"net/http"
	"strconv"
	"strings"

	"nhooyr.io/websocket"
)

func Handler(schema Schema, playground http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		Handle(r.Context(), schema, playground, w, r)
	})
}

func Handle(ctx context.Context, schema Schema, playground http.Handler, w http.ResponseWriter, r *http.Request) {
	if strings.EqualFold(r.Header.Get("Upgrade"), "websocket") {
		conn, err := websocket.Accept(w, r, wsOpts)
		if err != nil {
			return
		}
		defer conn.Close(websocket.StatusNormalClosure, "")

		ws := ws{ctx: ctx, conn: conn, schema: schema}
		ws.run()

		return
	}

	params, err := formParams(r.URL.Query())
	if err != nil {
		http.Error(w, "failed parsing query variables", http.StatusBadRequest)
		return
	}
	if params.Query == "" {
		ct, _, _ := mime.ParseMediaType(r.Header.Get("Content-Type"))

		switch ct {
		case "application/graphql":
			var b strings.Builder
			_, err := io.Copy(&b, r.Body)
			if err != nil {
				http.Error(w, "failed reading request body", http.StatusBadRequest)
				return
			}
			params.Query = b.String()
		case "application/x-www-form-urlencoded":
			err = r.ParseForm()
			if err != nil {
				http.Error(w, "failed parsing request body", http.StatusBadRequest)
				return
			}
			params, err = formParams(r.Form)
			if err != nil {
				http.Error(w, "failed parsing request variables", http.StatusBadRequest)
				return
			}
		case "multipart/form-data":
			err = r.ParseMultipartForm(32 << 20)
			if err != nil {
				http.Error(w, "failed reading request body", http.StatusBadRequest)
				return
			}

			err = json.Unmarshal([]byte(r.Form.Get("operations")), &params)
			if err != nil {
				http.Error(w, "failed parsing request operations", http.StatusBadRequest)
				return
			}

			var opMap map[string][]string
			err = json.Unmarshal([]byte(r.Form.Get("map")), &opMap)
			if err != nil {
				http.Error(w, "failed parsing request operations map", http.StatusBadRequest)
				return
			}
			for name, paths := range opMap {
				for _, p := range paths {
					parts := strings.Split(p, ".")

					f, _, err := r.FormFile(name)
					if err != nil {
						http.Error(w, "failed loading file '"+name+"' specified in operations map", http.StatusBadRequest)
						return
					}

					var v interface{} = params
					for i, part := range parts {
						switch v2 := v.(type) {
						case Params:
							switch part {
							case "variables":
								v = v2.Variables
							}
						case map[string]interface{}:
							if i == len(parts)-1 {
								v2[part] = f
							} else {
								v = v2[part]
							}
						case []interface{}:
							n, err := strconv.Atoi(part)
							if err != nil {
								http.Error(w, "failed parsing path index in operations map", http.StatusBadRequest)
								return
							}
							if i == len(parts)-1 {
								v2[n] = f
							} else {
								v = v2[n]
							}
						}
					}
				}
			}
		default:
			b, err := io.ReadAll(r.Body)
			if err != nil {
				http.Error(w, "failed reading request body", http.StatusBadRequest)
				return
			}

			if len(b) > 0 {
				err = json.Unmarshal(b, &params)
				if err != nil {
					http.Error(w, "failed parsing request body", http.StatusBadRequest)
					return
				}
			}
		}
	}

	switch r.Header.Get("Accept") {
	case "text/event-stream":
		wf := w.(http.Flusher)

		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.WriteHeader(http.StatusOK)
		wf.Flush()

		done := ctx.Done()

		e := json.NewEncoder(w)
		e.SetEscapeHTML(false)

		c := schema.Subscribe(ctx, params)
	loop:
		for {
			select {
			case <-done:
				break loop
			case res, ok := <-c:
				if !ok {
					break loop
				}
				w.Write([]byte("event: next\ndata: "))
				e.Encode(res)
				w.Write([]byte("\n"))
				wf.Flush()
			}
		}

		w.Write([]byte("event: complete\ndata: \n\n"))
		wf.Flush()

		return
	}

	if params.Query == "" && playground != nil && r.Method == http.MethodGet {
		playground.ServeHTTP(w, r)
		return
	}

	res := schema.Do(ctx, params)

	b, err := json.Marshal(res)
	if err != nil {
		http.Error(w, "failed to marshal response", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json; charset=utf-8")

	w.Write(b)
}
