package websub

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"encoding/xml"
	"errors"
	"fmt"
	"hash"
	"io"
	"mime"
	"net/http"
	url2 "net/url"
	"strings"
	"sync"

	"github.com/tomnomnom/linkheader"
	xhttp "lelux.net/x/net/http"
)

type Client struct {
	Client *http.Client

	subscriptions sync.Map

	CallbackURL string

	Content chan []byte
}

var ErrDiscoverMissing = errors.New("websub: hub or topic missing from discover response")

type discoverXml struct {
	Link []struct {
		Href string `xml:"href,attr"`
		Rel  string `xml:"rel,attr"`
	} `xml:"link"`
}

func (c *Client) Discover(url string) (hub string, topic string, err error) {
	client := c.Client
	if client == nil {
		client = http.DefaultClient
	}
	res, err := client.Get(url)
	if err != nil {
		return "", "", err
	}
	defer res.Body.Close()

	for _, link := range linkheader.ParseMultiple(res.Header["Link"]) {
		switch link.Rel {
		case "hub":
			hub = link.URL
		case "self":
			topic = link.URL
		}
	}

	if hub == "" || topic == "" {
		var ct string
		ct, _, err = mime.ParseMediaType(res.Header.Get("Content-Type"))
		if err != nil {
			return
		}

		switch ct {
		case "text/xml":
			var b []byte
			b, err = io.ReadAll(res.Body)
			if err != nil {
				return
			}

			var x discoverXml
			err = xml.Unmarshal(b, &x)
			if err != nil {
				return
			}

			for _, link := range x.Link {
				switch link.Rel {
				case "hub":
					hub = link.Href
				case "self":
					topic = link.Href
				}
			}
		}
	}

	if hub == "" || topic == "" {
		return "", "", ErrDiscoverMissing
	}

	var hubU *url2.URL
	hubU, err = res.Request.URL.Parse(hub)
	if err != nil {
		return
	}
	hub = hubU.String()

	var topicU *url2.URL
	topicU, err = res.Request.URL.Parse(topic)
	if err != nil {
		return
	}
	topic = topicU.String()
	return
}

func (c *Client) req(hub string, body string) error {
	req, err := http.NewRequest(http.MethodPost, hub, strings.NewReader(body))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := c.Client
	if client == nil {
		client = http.DefaultClient
	}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusAccepted {
		return xhttp.UnexpectedStatus(res.Status)
	}

	return nil
}

type subscription struct {
	hub    string
	secret []byte
}

func genSecret() ([]byte, error) {
	b := make([]byte, 30)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	enc := base64.RawURLEncoding
	b64 := make([]byte, enc.EncodedLen(len(b)))
	enc.Encode(b64, b)
	return b64, nil
}

func (c *Client) Subscribe(hub, topic string) error {
	secret, err := genSecret()
	if err != nil {
		return err
	}
	c.subscriptions.Store(topic, subscription{hub: hub, secret: secret})

	queryTopic := url2.QueryEscape(topic)
	callback := fmt.Sprintf("%s?topic=%s", c.CallbackURL, queryTopic)
	return c.req(hub, fmt.Sprintf("hub.callback=%s&hub.mode=subscribe&hub.topic=%s&hub.secret=%s", url2.QueryEscape(callback), queryTopic, secret))
}

func (c *Client) Unsubscribe(topic string) error {
	subV, ok := c.subscriptions.Load(topic)
	if !ok {
		return nil
	}
	sub := subV.(subscription)
	return c.req(sub.hub, fmt.Sprintf("hub.callback=%s&hub.mode=unsubscribe&hub.topic=%s", url2.QueryEscape(c.CallbackURL), url2.QueryEscape(topic)))
}

func (c *Client) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		q := r.URL.Query()
		switch q.Get("hub.mode") {
		case "denied":
			// TODO
			return
		case "subscribe":
			_, ok := c.subscriptions.Load(q.Get("hub.topic"))
			if !ok {
				http.Error(w, "topic not subscribed", http.StatusNotFound)
				return
			}

			io.WriteString(w, q.Get("hub.challenge"))
		case "unsubscribe":
			_, ok := c.subscriptions.Load(q.Get("hub.topic"))
			if ok {
				http.Error(w, "topic still subscribed", http.StatusNotFound)
				return
			}

			io.WriteString(w, q.Get("hub.challenge"))
		default:
			http.Error(w, "mode not supported", http.StatusBadRequest)
			return
		}
	case http.MethodPost:
		if c.Content == nil {
			return
		}

		q := r.URL.Query()

		subV, ok := c.subscriptions.Load(q.Get("topic"))
		if !ok {
			http.Error(w, "topic not subscribed", http.StatusNotFound)
			return
		}
		sub := subV.(subscription)

		signature := r.Header.Get("X-Hub-Signature")
		if signature == "" {
			http.Error(w, "missing signature", http.StatusUnauthorized)
			return
		}

		i := strings.IndexByte(signature, '=')
		if i == -1 {
			http.Error(w, "missing = in signature", http.StatusBadRequest)
			return
		}

		var h func() hash.Hash
		switch signature[:i] {
		case "sha1":
			h = sha1.New
		case "sha256":
			h = sha256.New
		case "sha384":
			h = sha512.New384
		case "sha512":
			h = sha512.New
		default:
			http.Error(w, "unknown signature method", http.StatusNotImplemented)
			return
		}

		sigMac, err := hex.DecodeString(signature[i+1:])
		if err != nil {
			http.Error(w, "signature not in hex format", http.StatusBadRequest)
			return
		}

		b, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "error reading request body", http.StatusBadRequest)
			return
		}

		mac := hmac.New(h, sub.secret)
		mac.Write(b)
		expectedMac := mac.Sum(nil)

		if hmac.Equal(expectedMac, sigMac) {
			c.Content <- b
		}

		w.WriteHeader(http.StatusNoContent)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
