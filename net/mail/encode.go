package mail

import (
	"encoding/base64"
	"io"
	"mime/quotedprintable"
	"unicode"
)

func encoding(b []byte) string {
	quotedLen := len(b)
	ascii := true
	for i := 0; i < len(b); i++ {
		if b[i] > unicode.MaxASCII {
			ascii = false
			quotedLen += 2
		} else if b[i] == '=' {
			quotedLen += 2
		}
	}

	if ascii {
		return "7bit"
	}

	baseLen := base64.StdEncoding.EncodedLen(len(b))
	if baseLen < quotedLen {
		return "base64"
	} else {
		return "quoted-printable"
	}
}

func encode(w io.Writer, enc string, b []byte) error {
	switch enc {
	case "7bit", "8bit":
		// TODO: Handle line breaks
		_, err := w.Write(b)
		return err
	case "binary":
		_, err := w.Write(b)
		return err
	case "quoted-printable":
		wq := quotedprintable.NewWriter(w)
		_, err := wq.Write(b)
		if err != nil {
			return err
		}
		return wq.Close()
	case "base64":
		wj := &joinWriter{w, 0, 76, []byte{'\r', '\n'}}
		wb := base64.NewEncoder(base64.StdEncoding, wj)
		_, err := wb.Write(b)
		if err != nil {
			return err
		}
		return wb.Close()
	default:
		panic("unknown encoding type")
	}
}

type joinWriter struct {
	w    io.Writer
	i    int
	len  int
	join []byte
}

func (w *joinWriter) Write(p []byte) (int, error) {
	var fullN int
	for len(p) > 0 {
		pl := w.len - w.i
		if pl > len(p) {
			pl = len(p)
		}

		n, err := w.w.Write(p[:pl])
		w.i += n
		fullN += n
		if err != nil {
			return fullN, err
		}

		p = p[pl:]

		if w.i == w.len {
			_, err = w.w.Write(w.join)
			if err != nil {
				return fullN, err
			}
			w.i = 0
		}
	}
	return fullN, nil
}
