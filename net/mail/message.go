package mail

import (
	"fmt"
	"io"
	"mime/multipart"
	"net/mail"
	"net/textproto"
	"strings"
	"time"

	xtextproto "lelux.net/x/net/textproto"
)

type Message struct {
	Subject string

	From mail.Address
	To   []mail.Address
	CC   []mail.Address
	BCC  []mail.Address

	Header textproto.MIMEHeader

	Body string

	Attachments []Attachment
}

type Attachment struct {
	Header textproto.MIMEHeader
	Name   string
	Body   io.Reader
}

func joinAddresses(addrs []mail.Address) string {
	if len(addrs) == 0 {
		return addrs[0].String()
	}
	var b strings.Builder
	b.WriteString(addrs[0].String())
	for _, addr := range addrs[1:] {
		b.WriteString(addr.String())
	}
	return b.String()
}

var quoteEscaper = strings.NewReplacer(`\`, `\\`, `"`, `\"`)

func (m Message) WriteTo(w io.Writer) (int64, error) {
	if m.Header == nil {
		m.Header = textproto.MIMEHeader{}
	}

	if m.Header.Get("MIME-Version") == "" {
		m.Header.Set("MIME-Version", "1.0")
	}
	if m.Header.Get("Date") == "" {
		m.Header.Set("Date", time.Now().Format(time.RFC1123Z))
	}

	if m.Subject != "" {
		m.Header.Set("Subject", m.Subject)
	}

	if m.From.Address != "" {
		m.Header.Set("From", m.From.String())
	}
	if len(m.To) > 0 {
		m.Header.Set("To", joinAddresses(m.To))
	}

	if len(m.CC) > 0 {
		m.Header.Set("Cc", joinAddresses(m.To))
	}
	if len(m.BCC) > 0 {
		m.Header.Set("Bcc", joinAddresses(m.To))
	}

	cw := &countWriter{w, 0}

	if len(m.Attachments) == 0 {
		body := []byte(m.Body)
		enc := encoding(body)
		m.Header.Set("Content-Transfer-Encoding", enc)

		_, err := xtextproto.MIMEHeader(m.Header).WriteTo(cw)
		if err != nil {
			return cw.i, err
		}

		err = encode(cw, enc, body)
		if err != nil {
			return cw.i, err
		}
	} else {
		mw := multipart.NewWriter(cw)
		m.Header.Set("Content-Type", "multipart/mixed; boundary="+mw.Boundary())

		_, err := xtextproto.MIMEHeader(m.Header).WriteTo(cw)
		if err != nil {
			return cw.i, err
		}

		if m.Body != "" {
			body := []byte(m.Body)
			enc := encoding(body)
			h := textproto.MIMEHeader{
				"Content-Type":              []string{"text/plain"},
				"Content-Transfer-Encoding": []string{enc}}
			pw, err := mw.CreatePart(h)
			if err != nil {
				return cw.i, err
			}
			err = encode(pw, enc, body)
			if err != nil {
				return cw.i, err
			}
		}

		for _, a := range m.Attachments {
			body, err := io.ReadAll(a.Body)
			if err != nil {
				return cw.i, err
			}
			enc := encoding(body)

			if a.Header == nil {
				a.Header = textproto.MIMEHeader{}
			}

			disposition := fmt.Sprintf(`attachment; filename="%s"`, quoteEscaper.Replace(a.Name))
			a.Header.Set("Content-Disposition", disposition)

			a.Header.Set("Content-Transfer-Encoding", enc)
			pw, err := mw.CreatePart(a.Header)
			if err != nil {
				return cw.i, err
			}
			err = encode(pw, enc, body)
			if err != nil {
				return cw.i, err
			}
		}

		err = mw.Close()
		if err != nil {
			return cw.i, err
		}
	}

	return cw.i, nil
}

type countWriter struct {
	w io.Writer
	i int64
}

func (w *countWriter) Write(p []byte) (int, error) {
	n, err := w.w.Write(p)
	w.i += int64(n)
	return n, err
}
