package http

import (
	"fmt"
)

type UnexpectedStatus string

func (e UnexpectedStatus) Error() string {
	return fmt.Sprintf("net/http: unexpected status: %s", string(e))
}
