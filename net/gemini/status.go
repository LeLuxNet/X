package gemini

const (
	StatusInput          = 10
	StatusSensitiveInput = 11

	StatusSuccess = 20

	StatusRedirectTemporary = 30
	StatusRedirectPermanent = 31

	StatusTemporaryFailure  = 40
	StatusServerUnavailable = 41
	StatusCGIError          = 42
	StatusProxyError        = 43
	StatusSlowDown          = 44

	StatusPermanentFailure    = 50
	StatusNotFound            = 51
	StatusGone                = 52
	StatusProxyRequestRefused = 53
	StatusBadRequest          = 59

	StatusClientCertificateRequired = 60
	StatusCertificateNotAuthorised  = 61
	StatusCertificateNotValid       = 62
)

var statusText = map[int]string{
	StatusInput:          "Input",
	StatusSensitiveInput: "Sensitive Input",

	StatusSuccess: "Success",

	StatusRedirectTemporary: "Redirect - Temporary",
	StatusRedirectPermanent: "Redirect - Permanent",

	StatusTemporaryFailure:  "Temporary Failure",
	StatusServerUnavailable: "Server Unavailable",
	StatusCGIError:          "CGI Error",
	StatusProxyError:        "Proxy Error",
	StatusSlowDown:          "Slow Down",

	StatusPermanentFailure:    "Permanent Failure",
	StatusNotFound:            "Not Found",
	StatusGone:                "Gone",
	StatusProxyRequestRefused: "Proxy Request Refused",
	StatusBadRequest:          "Bad Request",

	StatusClientCertificateRequired: "Client Certificate Required",
	StatusCertificateNotAuthorised:  "Certificate Not Authorised",
	StatusCertificateNotValid:       "Certificate Not Valid",
}

func StatusText(code int) string {
	return statusText[code]
}
