package gemini

import (
	"context"
	"crypto/tls"
	"errors"
	"net/url"
	urlpkg "net/url"
)

type Request struct {
	URL *url.URL

	Cert *tls.Certificate

	ctx context.Context
}

func NewRequest(url string) (*Request, error) {
	return NewRequestWithContext(context.Background(), url)
}

func NewRequestWithContext(ctx context.Context, url string) (*Request, error) {
	if ctx == nil {
		return nil, errors.New("net/gemini: nil Context")
	}
	u, err := urlpkg.Parse(url)
	if err != nil {
		return nil, err
	}
	return &Request{URL: u, ctx: ctx}, nil
}
