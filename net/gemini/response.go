package gemini

import (
	"crypto/tls"
	"io"
)

type Response struct {
	StatusCode int
	Meta       string

	Body io.ReadCloser

	Request *Request

	TLS tls.ConnectionState
}
