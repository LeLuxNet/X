package gemini

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"math"
	"net"
	"strconv"
)

type Client struct {
	CheckRedirect func(r *Request, via []*Request) error
}

var ErrUseLastResponse = errors.New("net/gemini: use last response")

func defaultCheckRedirect(req *Request, via []*Request) error {
	if len(via) >= 5 {
		return errors.New("stopped after 5 redirects")
	}
	return nil
}

var DefaultClient = Client{}

func Fetch(url string) (*Response, error) {
	return DefaultClient.Fetch(url)
}

func (c Client) Fetch(url string) (*Response, error) {
	res, err := NewRequest(url)
	if err != nil {
		return nil, err
	}
	return c.Do(res)
}

func (c Client) Do(req *Request) (*Response, error) {
	var reqs []*Request
	for {
		res, err := c.do(req)
		if err != nil {
			return nil, err
		}

		if 30 <= res.StatusCode && res.StatusCode <= 39 {
			reqs = append(reqs, req)
			req2 := *req
			req = &req2

			req.URL, err = req.URL.Parse(res.Meta)
			if err != nil {
				return res, err
			}

			check := c.CheckRedirect
			if check == nil {
				check = defaultCheckRedirect
			}
			err := check(req, reqs)
			if err == ErrUseLastResponse {
				return res, nil
			}
			if err != nil {
				return nil, err
			}
		} else {
			return res, err
		}
	}
}

func (c Client) do(req *Request) (*Response, error) {
	host := req.URL.Host
	if port := req.URL.Port(); port == "" {
		host = net.JoinHostPort(host, "1965")
	}

	conf := &tls.Config{
		MinVersion:         tls.VersionTLS12,
		InsecureSkipVerify: true}
	if req.Cert != nil {
		conf.Certificates = []tls.Certificate{*req.Cert}
	}

	dialer := tls.Dialer{Config: conf}
	conn, err := dialer.DialContext(req.ctx, "tcp", host)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			conn.Close()
		}
	}()

	// TODO: Check cert

	res := Response{
		Request: req,
		TLS:     conn.(*tls.Conn).ConnectionState()}

	_, err = io.WriteString(conn, req.URL.String())
	if err != nil {
		return nil, err
	}
	_, err = io.WriteString(conn, "\r\n")
	if err != nil {
		return nil, err
	}

	lr := &io.LimitedReader{R: conn, N: 3 + 1024 + 2}
	r := bufio.NewReaderSize(lr, 1024+2)

	status, err := r.ReadString(' ')
	if err != nil {
		return nil, err
	}
	if len(status) != 3 {
		return nil, fmt.Errorf("net/gemini: malformed status code %s", status)
	}
	res.StatusCode, err = strconv.Atoi(status[:2])
	if err != nil || 10 > res.StatusCode || res.StatusCode >= 70 {
		return nil, fmt.Errorf("net/gemini: malformed status code %s: %w", status, err)
	}

	b, err := r.ReadSlice('\n')
	if err != nil {
		return nil, err
	}
	if len(b) < 2 || b[len(b)-2] != '\r' {
		return nil, errors.New("net/gemini: malformed header")
	}
	b = b[:len(b)-2]
	if bytes.IndexByte(b, '\r') != -1 {
		return nil, errors.New("net/gemini: malformed header")
	}
	res.Meta = string(b)

	if 20 <= res.StatusCode && res.StatusCode <= 29 {
		if res.Meta == "" {
			res.Meta = "text/gemini; charset=utf-8"
		}
		lr.N = math.MaxInt64
		res.Body = readCloser{r, conn}
	} else {
		conn.Close()
		res.Body = nopBody{}
	}

	return &res, nil
}

type readCloser struct {
	io.Reader
	io.Closer
}

type nopBody struct{}

func (r nopBody) Read(p []byte) (int, error) {
	return 0, io.EOF
}

func (r nopBody) Close() error {
	return nil
}
