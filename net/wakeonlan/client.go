package wakeonlan

import (
	"errors"
	"net"
)

var magicHeader = []byte{0xff, 0xff, 0xff, 0xff, 0xff, 0xff}

const (
	magicMacLength = 6
	magicMacRepeat = 16
)

var (
	ErrFormat = errors.New("wakeonlan: hardware address not in IEEE EUI-48 format")
)

func build(mac net.HardwareAddr) ([]byte, error) {
	if len(mac) != magicMacLength {
		return nil, ErrFormat
	}

	b := make([]byte, len(magicHeader)+magicMacLength*magicMacRepeat)

	copy(b, magicHeader)

	for i := len(magicHeader); i < len(b); i += magicMacLength {
		copy(b[i:], mac)
	}

	return b, nil
}

// Sends a Wake-on-LAN packet to a specified device.
// The address is optional and can be left empty.
func Wake(mac net.HardwareAddr, address string) error {
	b, err := build(mac)
	if err != nil {
		return err
	}

	if address == "" {
		address = "255.255.255.255:9"
	} else if address[0] == ':' {
		address = "255.255.255.255" + address
	}

	conn, err := net.Dial("udp", address)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Write(b)

	return err
}
