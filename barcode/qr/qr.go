package qr

import (
	"image"

	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode/detector"
	"github.com/skip2/go-qrcode"
	"lelux.net/x/barcode"
)

type RecoveryLevel int

const (
	L = RecoveryLevel(qrcode.Low)
	M = RecoveryLevel(qrcode.Medium)
	Q = RecoveryLevel(qrcode.High)
	H = RecoveryLevel(qrcode.Highest)
)

type DetectResult struct {
	Bits barcode.BitMatrix
}

func Detect(matrix barcode.BitMatrix) (DetectResult, error) {
	res, err := detector.NewDetector((*gozxing.BitMatrix)(&matrix)).Detect(nil)
	if err != nil {
		return DetectResult{}, err
	}
	return DetectResult{
		Bits: barcode.BitMatrix(*res.GetBits()),
	}, nil
}

func Encode(content string, level RecoveryLevel) (image.Image, error) {
	qr, err := qrcode.New(content, qrcode.RecoveryLevel(level))
	if err != nil {
		return nil, err
	}

	return qr.Image(0), nil
}
