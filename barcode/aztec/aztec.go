package aztec

import (
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/aztec/detector"
	"lelux.net/x/barcode"
)

type DetectResult struct {
	Bits barcode.BitMatrix

	Compact    bool
	Datablocks int
	Layers     int
}

func Detect(matrix barcode.BitMatrix, mirror bool) (DetectResult, error) {
	res, err := detector.NewDetector((*gozxing.BitMatrix)(&matrix)).Detect(mirror)
	if err != nil {
		return DetectResult{}, err
	}
	return DetectResult{
		Bits: barcode.BitMatrix(*res.GetBits()),

		Compact:    res.IsCompact(),
		Datablocks: res.GetNbDatablocks(),
		Layers:     res.GetNbLayers(),
	}, nil
}
