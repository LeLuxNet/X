package barcode

import (
	"image"

	"github.com/makiuchi-d/gozxing"
)

func luminanceSource(img image.Image) gozxing.LuminanceSource {
	if img, ok := img.(*image.YCbCr); ok {
		return ycbcrSource{img}
	}

	return gozxing.NewLuminanceSourceFromImage(img)
}

type ycbcrSource struct {
	*image.YCbCr
}

func (ls ycbcrSource) GetRow(y int, row []byte) ([]byte, error) {
	yRow := (y - ls.Rect.Min.Y) * ls.YStride
	from := yRow + ls.Rect.Min.X
	to := yRow + ls.Rect.Max.X

	width := to - from
	if len(row) < width {
		row = make([]byte, width)
	}

	copy(row, ls.Y[from:to])
	return row, nil
}

func (ls ycbcrSource) GetMatrix() []byte {
	if ls.Rect.Min.X == 0 &&
		ls.Rect.Max.X == ls.YStride {
		from := ls.Rect.Min.Y * ls.YStride
		to := ls.Rect.Max.Y * ls.YStride
		return ls.Y[from:to]
	}

	b := make([]byte, ls.Rect.Dx()*ls.Rect.Dy())
	i := 0
	for y := ls.Rect.Min.Y; y < ls.Rect.Max.Y; y++ {
		from := ls.YOffset(ls.Rect.Min.X, y)
		to := ls.YOffset(ls.Rect.Max.X, y)
		copy(b[i:], ls.Y[from:to])
		i += to - from
	}
	return b
}

func (ls ycbcrSource) GetWidth() int {
	return ls.Bounds().Dx()
}
func (ls ycbcrSource) GetHeight() int {
	return ls.Bounds().Dy()
}

func (ls ycbcrSource) IsCropSupported() bool {
	return true
}
func (ls ycbcrSource) Crop(left, top, width, height int) (gozxing.LuminanceSource, error) {
	crop := ls.SubImage(image.Rect(left, top, left+width, top+height))
	return luminanceSource(crop), nil
}

func (ls ycbcrSource) IsRotateSupported() bool {
	return false
}

func (ls ycbcrSource) Invert() gozxing.LuminanceSource {
	return gozxing.LuminanceSourceInvert(ls)
}

func (ls ycbcrSource) RotateCounterClockwise() (gozxing.LuminanceSource, error) {
	panic("Not implemented")
}

func (ls ycbcrSource) RotateCounterClockwise45() (gozxing.LuminanceSource, error) {
	panic("Not implemented")
}

func (ls ycbcrSource) String() string {
	return "YCbCr"
}
