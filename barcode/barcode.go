package barcode

import (
	"image"
	"image/color"

	"github.com/makiuchi-d/gozxing"
)

type BitMatrix gozxing.BitMatrix

func (m BitMatrix) Width() int {
	img := (gozxing.BitMatrix)(m)
	return img.GetWidth()
}

func (m BitMatrix) Height() int {
	img := (gozxing.BitMatrix)(m)
	return img.GetHeight()
}

func (m BitMatrix) Get(x, y int) bool {
	img := (gozxing.BitMatrix)(m)
	return img.Get(x, y)
}

func (m BitMatrix) ColorModel() color.Model {
	return color.GrayModel
}

func (m BitMatrix) Bounds() image.Rectangle {
	return image.Rect(0, 0, m.Width(), m.Height())
}

func (m BitMatrix) At(x, y int) color.Color {
	if m.Get(x, y) {
		return color.Gray{0}
	} else {
		return color.Gray{255}
	}
}

type Binarizer interface {
	BlackMatrix() (BitMatrix, error)
}

type binarizer struct {
	b gozxing.Binarizer
}

func (b binarizer) BlackMatrix() (BitMatrix, error) {
	m, err := b.b.GetBlackMatrix()
	if err != nil {
		return BitMatrix{}, err
	}
	return BitMatrix(*m), nil
}

func GlobalHistogramBinarizer(img image.Image) Binarizer {
	return binarizer{gozxing.NewGlobalHistgramBinarizer(luminanceSource(img))}
}

func HybridBinarizer(img image.Image) Binarizer {
	return binarizer{gozxing.NewHybridBinarizer(luminanceSource(img))}
}
