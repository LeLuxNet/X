package msgpack

import (
	"bytes"

	"github.com/vmihailenco/msgpack/v5"
)

func Marshal(v interface{}) ([]byte, error) {
	var buf bytes.Buffer
	e := msgpack.NewEncoder(&buf)
	e.UseCompactInts(true)
	e.UseCompactFloats(true)
	err := e.Encode(&v)
	return buf.Bytes(), err
}
