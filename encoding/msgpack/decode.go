// Package msgpack implements encoding and decoding of MessagePack as defined in
// https://github.com/msgpack/msgpack/blob/master/spec.md
package msgpack

import "github.com/vmihailenco/msgpack/v5"

func Unmarshal(data []byte, v interface{}) error {
	return msgpack.Unmarshal(data, v)
}
