package smile_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"testing"

	"lelux.net/x/encoding/smile"
)

var filenames = []string{
	"big-integer",
	"db100.xml",
	"json-org-sample1",
	"json-org-sample2",
	"json-org-sample3",
	"json-org-sample4",
	"json-org-sample5",
	"map-spain.xml",
	"ns-invoice100.xml",
	"ns-soap.xml",
	"numbers-fp-4k",
	"numbers-fp-64k",
	"numbers-int-4k",
	"numbers-int-64k",
	"test1",
	"test2",
	"test3",
	"unicode",
}

func TestDecode(t *testing.T) {
	for _, fn := range filenames {
		smileData, err := os.ReadFile("testdata/" + fn + ".smile")
		if err != nil {
			t.Error(fn, err)
			continue
		}

		var smileV interface{}
		err = smile.Unmarshal(smileData, &smileV)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		smileJson, err := json.Marshal(smileV)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		jsonF, err := os.Open("testdata/" + fn + ".json")
		if err != nil {
			t.Error(fn, err)
			continue
		}
		defer jsonF.Close()

		jsonD := json.NewDecoder(jsonF)
		if strings.HasPrefix(fn, "big-integer") {
			jsonD.UseNumber()
		}
		var jsonV interface{}
		err = jsonD.Decode(&jsonV)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		jsonJson, err := json.Marshal(jsonV)
		if err != nil {
			t.Error(fn, err)
			continue
		}

		if !bytes.Equal(smileJson, jsonJson) {
			t.Error(fn, "smile and json not equal")
			fmt.Println(string(smileJson), string(jsonJson))
		}
	}
}
