package smile

type shared []string

func (sPtr *shared) add(val string) {
	s := *sPtr
	if len(s) >= 1024 {
		s = s[:0]
	}
	s = append(s, val)
	*sPtr = s
}
